# <!-- coding: utf-8 -->
#
# quelques fonction pour l'atlas Rennes
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# le distance sampling
# https://www.biorxiv.org/content/biorxiv/early/2016/07/14/063891.full.pdf
# https://sites.google.com/site/wild8390/spring-2017/lecture-lab-schedule/week-4--abundance-estimation/unmarked-distance-sampling
# http://www.montana.edu/screel/teaching/bioe-440r-521/bioe-440-distance-sampling.html
# https://www.researchgate.net/publication/257557523_Random_versus_stratified_location_of_transects_or_points_in_distance_sampling_Theoretical_results_and_practical_considerations
# https://synergy.st-andrews.ac.uk/ds-manda/montrave-in-r/
#
# rm(list=ls()); source("geo/scripts/rennesatlas.R");ds_jour()
ds_jour <- function() {
  carp()
  library(tidyverse)
  session <<- '201804'
  ds_extrait_tous()
  session <<- '201806'
  ds_extrait_tous()
#  ds_test()
}
ds_install <- function() {
  carp()
  require(devtools)
  install_github("DistanceDevelopment/mrds")
  install_github("DistanceDevelopment/Distance")
  install.packages("gdata")
  devtools::session_info()
  options(width = 120)
}
#
# extraction d'un fichier pour test du distance sampling
ds_extrait <- function() {
  carp()
  library(tidyverse)
  points_internet_lire()
  df <- points.df %>%
    filter(email != "iledusud@gmail.com") %>%
    filter(Espece == "Merle noir") %>%
    dplyr::select(point, point_lat, point_lng, obs_lat, obs_lng, distance, espece, nombre) %>%
    glimpse()
  df_ecrirex(df)
}
# rm(list=ls()); source("geo/scripts/rennesatlas.R");ds_extrait_tous()
ds_extrait_tous <- function() {
  carp()
  library(tidyverse)
  df <- points_internet_lire(force=TRUE)
  carp("détermination des espèces à prendre en compte")
#  glimpse(df);stop('***')
  df1 <- df %>%
    group_by(espece) %>%
    filter(distance < 2)
#  View(df1);stop('***')
  df1 <- df %>%
    group_by(espece) %>%
    summarize(nb_donnees=n(), nb_oiseaux=sum(nombre), distances=sum(distance)) %>%
    filter(nb_donnees > 10) %>%
    glimpse()
  stat.df <- data.frame()
  for ( i in 1:nrow(df1) ) {
    stat.df <- ds_extrait_txt(df1[i, 'espece'], stat.df)
  }
  stat.df <- ds_extrait_txt('', stat.df)
#  View(stat.df)
  tex_df2table(stat.df, suffixe=session)
}
# rm(list=ls()); source("geo/scripts/rennesatlas.R");ds_extrait_txt('Moineau domestique')
# rm(list=ls()); source("geo/scripts/rennesatlas.R");ds_extrait_txt('Troglodyte mignon')
# rm(list=ls()); source("geo/scripts/rennesatlas.R");ds_extrait_txt('')
ds_extrait_txt <- function(Espece='Merle noir', stat.df) {
  carp()
  library(tidyverse)
  dsDir <<- 'C:/Users/Marc/Documents/My Distance Projects'
  points_internet_lire()
  carp("détermination des points à prendre en compte")
  p.df <- points.df %>%
    group_by(point) %>%
    summarize(nb=n())
  carp("nb_points: %s", nrow(p.df))
  surface <- 5 * 5 * nrow(p.df)
  p.df <- p.df %>%
    mutate(Region='Rennes', Surface=surface, Effort=1, distance='') %>%
    dplyr::select(Region, Surface, point, Effort, distance) %>%
    glimpse()
  carp("les données")
  df <- points.df

#
# pour l'espèce
  Espece <- as.character(Espece)
  if ( Espece != '' ) {
    df <- df %>%
      filter(espece == Espece) %>%
      glimpse()
  } else {
    Espece <- 'tous'
  }
#
# une donnée par oiseau
  df1 <- df[0, ]
  for ( i in 1:nrow(df) ) {
    nb <- as.numeric(df[i, 'nombre'])
    for ( j in 1:nb ) {
      df1 <- rbind(df1, df[i, ])
    }
  }

  if ( nrow(df1) < 10 ) {
    carp("*** Espece: %s", Espece)
    stop('***')
  }
  i <- nrow(stat.df) + 1
  stat.df[i, 'espece'] <- Espece
  stat.df[i, 'nb'] <- nrow(df1)
  stat.df[i, 'dmin'] <- round(min(df1$distance), 0)
  stat.df[i, 'dmax'] <- round(max(df1$distance), 0)
  stat.df[i, 'dmoy'] <- round(mean(df1$distance), 0)
  stat.df[i, 'dmed'] <- round(median(df1$distance), 0)
  stat.df[i, 'dvar'] <- round(var(df1$distance), 0)
  stat.df[i, 'dsd'] <- round(sd(df1$distance), 0)
#
# suppression des 5%
  brks <- quantile(df1$distance, seq(from=0, to=1, by=1/20), 0)
  stat.df[i, 'd5'] <- round(brks[[2]])
  stat.df[i, 'd95'] <- round(brks[[20]])
#  print(brks)
#  summary(cut(df1$distance, breaks=brks, include.lowest = TRUE))
#  glimpse(df1$distance); stop('****')
  df <- df1 %>%
#    filter(email != "iledusud@gmail.com") %>%
#    filter() %>%
    mutate(Region='Rennes', Surface=surface, Effort=1) %>%
    dplyr::select(Region, Surface, point, Effort, distance) %>%
    glimpse()
  carp("les points sans données")
  p1.df <- p.df %>%
    filter(! point %in% df$point) %>%
    glimpse()
  df <- rbind(df, p1.df)
#  return()
  dsn <- sprintf("%s/%s_%s.txt", dsDir, session, Espece)
  write.table(df, file=dsn, quote=FALSE, sep="\t", eol="\n", col.names=F, row.names=F)
  carp("dsn: %s", dsn)
  dsn <- sprintf("%s/ds/%s_%s.xlsx", varDir, session, Espece)
  writexl::write_xlsx(df, dsn)
  carp("dsn: %s", dsn)
  return(invisible(stat.df))
}
# https://github.com/DistanceDevelopment/Distance/blob/master/R/Distance-package.R
# source("geo/scripts/rennesatlas.R");ds_test()
ds_test <- function() {
  carp()
  library(tidyverse)
  library(Distance)
  dsn <- sprintf("%s/%s.xlsx", texDir, 'ds_extrait')
  df <- read_excel(dsn, col_names = TRUE)
  glimpse(df)
}