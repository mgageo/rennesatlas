#!/bin/sh
# <!-- coding: utf-8 -->
#T fb : faune-bretagne
# auteur: Marc Gauthier
[ -f ../win32/scripts/misc.sh ] && . ../win32/scripts/misc.sh
[ -f ../win32/scripts/misc_pkg.sh ] && . ../win32/scripts/misc_pkg.sh
#f CONF:
CONF() {
  LOG "CONF debut"
  ENV
  CFG="VILAINEAVAL"
  [ -d "${CFG}" ] || mkdir "${CFG}"
  varDir="/d/bvi35/CouchesVilaineAval"
  varDir="/d/web.var/geo/fb"
  LOG "CONF fin"
}

#F e: edition des principaux fichiers
e() {
  LOG "e debut"
  E scripts/fb.sh
  for f in scripts/fb*.R ; do
    E $f
  done
  ( cd "${varDir}"; explorer . &)
  LOG "e fin"
}
FB_export() {
  _ENV_R
  ( cd ..; R --vanilla -e "source('geo/scripts/fb.R'); export_donnees();" )
#  ( cd ..; R --vanilla -e "source('geo/scripts/fb.R'); fusion_donnees();" )
}
#F GIT: pour mettre à jour le dépot git
GIT() {
  LOG "GIT debut"
  _git_lst
  bash ../win32/scripts/git.sh INIT $*
  LOG "GIT fin"
}
#f _git_lst: la liste des fichiers pour le dépot
_git_lst() {
  Local="${DRIVE}/web/geo"; Depot=fb; Remote=frama
  export Local
  export Depot
  export Remote
  cat  <<'EOF' > /tmp/_git.lst
scripts/fb.sh
EOF
  _ENV_R
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/fb.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/visionature.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/stoc.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/epoc.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  cat /tmp/_git.lst > /tmp/git.lst

  cat  <<'EOF' > /tmp/README.md
# fb : Faune-Bretagne

Scripts en environnement Windows 10 : MinGW R Texlive

Ces scripts exploitent des données en provenance :
- de Faune-Bretagne;
- de GéoBretagne et l'IGN pour la cartographie;
- de la LPO pour le protocole EPOC-ODF

## Scripts R
Ces scripts sont dans le dossier "scripts".
Les point d'entrée sont :
- stoc.R : pour le protocole STOC
- epoc.R : pour le format EPC dont EPOC-ODF
- fb.R : divers traitements
- visionature.R : divers traitements

## Tex
Le langage Tex est utilisé pour la production des documents.

Texlive est l'environnement utilisé.


EOF
}
[ $# -eq 0 ] && ( HELP )
CONF
while [ "$1" != "" ]; do
  case $1 in
    -c | --conf )
      shift
      Conf=$1
      ;;
    * )
      $*
      exit 1
  esac
  shift
done