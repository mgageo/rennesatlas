# <!-- coding: utf-8 -->
#
# quelques fonctions d'utilisation/préparation des couches OGC
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# source("geo/scripts/couches.R");opendatarennes_dl();
opendatarennes_dl <- function(force = TRUE) {
  library(rgdal, quietly = TRUE)
  library(sf)
  dl_dir <- sprintf("%s/opendatarennes", target_dir)
  dir.create(dl_dir, showWarnings = FALSE, recursive = TRUE)
  depot_dir <- 'https://public.sig.rennesmetropole.fr/ressources/donnees/referentiels'
  carp("dl_dir %s", dl_dir)
  les_url <- read.table(text="url
cartographie_generale_shp_lambcc48.zip
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="")
  for(i in 1:nrow(les_url) ) {
    url <- sprintf('%s/%s', depot_dir, les_url$url[i])
    zip <- sub(".*/", "", url)
    carp("zip:%s %s", zip, url)
    file <- sprintf("%s/%s", dl_dir, zip)
    if( ! file.exists(file) || force == TRUE) {
 #     download.file(url, file, quiet = FALSE, mode = "wb")
    }
    zip_dir <- sub("\\..*$", "", zip)
    zip_dir <- sprintf("%s/%s", dl_dir, zip_dir)
    carp("zip_dir: %s", zip_dir)
    if( ! file.exists(zip_dir) || force == TRUE) {
      unzip(file, overwrite = TRUE, junkpaths = TRUE, exdir = zip_dir, unzip = "internal", setTimes = TRUE)
    }
#    next
    files <- list.files(zip_dir, pattern = "\\.shp$", full.names = TRUE, ignore.case = TRUE)
    if ( length(files) < 1) {
      carp("pas de .shp dans %s", zip_dir)
      next;
    }
    for (dsn in files) {
      layers <- ogrListLayers(dsn) %>%
        glimpse()
      nc <- st_read(dsn, layers)
      dsn <- sprintf("%s/%s.geojson", webDir, layers)
      st_write(st_transform(nc, 4326), dsn, delete_dsn=TRUE, driver='GeoJSON')
    }
  }
}
#
# source("geo/scripts/couches.R");opendatarennes_dl();
opendatarennes_dl_2018 <- function() {
  library(rgdal, quietly = TRUE)
  dl_dir <- sprintf("%s/opendatarennes", target_dir)
  dir.create(dl_dir, showWarnings = FALSE, recursive = TRUE)
  print(sprintf("opendatarennes_dl() dl_dir %s", dl_dir))
  les_url <- read.table(text="url
http://www.data.rennes-metropole.fr/fileadmin/user_upload/data/data_sig/referentiels/cartographie_generale/cartographie_generale_shp_lamb93.zip
http://www.data.rennes-metropole.fr/fileadmin/user_upload/data/data_sig/citoyennete/sous_quartiers_vdr/sous_quartiers_vdr_shp_lamb93.zip
http://www.data.rennes-metropole.fr/fileadmin/user_upload/data/data_sig/referentiels/voies_adresses/voies_adresses_shp_lamb93.zip
http://www.data.rennes-metropole.fr/fileadmin/user_upload/data/data_sig/equip_sport_jeux_loisirs/jardins_familiaux/jardins_familiaux_shp_lamb93.zip
http://www.data.rennes-metropole.fr/fileadmin/user_upload/data/data_sig/referentiels/communes_rm/communes_rm_shp_lamb93.zip
http://www.data.rennes-metropole.fr/fileadmin/user_upload/data/data_sig/equip_sport_jeux_loisirs/jardins_familiaux/jardins_familiaux_shp_lamb93.zip
http://www.data.rennes-metropole.fr/fileadmin/user_upload/data/data_sig/altitude/mnt2004/35238_rennes/mnt2004_35238_rennes_isolignes_shp_lamb93.zip
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="")
  for(i in 1:nrow(les_url) ) {
    url <- les_url$url[i]
    zip <- sub(".*/", "", url)
    print(sprintf("opendatarennes_dl() zip:%s %s", zip, url))
    file <- sprintf("%s/%s", dl_dir, zip)
    if( ! file.exists(file) ) {
      download.file(url, file, quiet = FALSE, mode = "wb")
    }
    zip_dir  <-sub("\\..*$", "", zip)
    zip_dir <- sprintf("%s/%s", dl_dir, zip_dir)
    print(sprintf("opendatarennes_dl() zip_dir:%s", zip_dir))
    if( ! file.exists(zip_dir) ) {
      unzip(file, overwrite = TRUE,junkpaths = TRUE, exdir = zip_dir, unzip = "internal", setTimes = TRUE)
    }
    next
    files <- list.files(zip_dir, pattern = "\\.shp$", full.names = TRUE, ignore.case = TRUE)
    if ( length(files) != 1) {
      print(sprintf("opendatarennes() .shp"))
      next;
    }
    dsn <- files[1]
    layers <- ogrListLayers(dsn)
    print(sprintf("opendatarennes_dl() layers:%s", layers))
#    print(sprintf("opendatarennes() info:%s", OGRSpatialRef(dsn,layers)))
    sp <- readOGR(dsn,layers)
    print(summary(sp))
  }
}
# https://cran.r-project.org/web/packages/rgdal/vignettes/OGR_shape_encoding.pdf
opendatarennes_lire <- function(couche, test=TRUE) {
  library(rgdal, quietly = TRUE)
  spdf <- opendatarennes_lire_couche('sous_quartiers_vdr_shp_lamb93', 'sous_quartiers_vdr')
#  spdf@data$NOMSQUART  <- iconv(spdf@data$NOMSQUART , 'latin1','UTF-8')
  if ( test ) {
    print(Sys.getlocale("LC_CTYPE"))
    plot(spdf, border= "black", add=FALSE, lwd=3)
    print(head(spdf@data))
    text(coordinates(spdf), labels=spdf@data$NOMSQUART, cex=1, col="black")
  }
  return(spdf)
}
opendatarennes_lire_couche <- function(depot, couche) {
  dl_dir <- sprintf("%s/opendatarennes", target_dir)
  dsn <- sprintf("%s/%s/%s.shp", dl_dir, depot, couche)
  print(sprintf("opendatarennes_lire_couche() dsn: %s", dsn))
  dfsp <- readOGR(dsn, layer=couche, stringsAsFactors=FALSE)
  return(dfsp)
}
# source("geo/scripts/couches.R");opendatarennes_geojson_dl();
opendatarennes_geojson_dl <- function(force = FALSE) {
  library(stringr)
  dl_dir <- sprintf("%s/bvi35/CouchesRennesAtlas/opendatarennes", Drive)
  dl_dir <- sprintf("%s/bvi35/CouchesRennesMetropole", Drive)
  dir.create(dl_dir, showWarnings = FALSE, recursive = TRUE)
  carp("dl_dir %s", dl_dir)
  les_url <- read.table(text="url
https://data.rennesmetropole.fr/explore/dataset/arbres-d-alignement-rennes/download/?format=geojson&timezone=Europe/Berlin&lang=fr
https://data.rennesmetropole.fr/explore/dataset/arbres-dornement-rennes/download/?format=geojson&timezone=Europe/Berlin&lang=fr
https://data.rennesmetropole.fr/explore/dataset/surfaces-enherbees-rennes/download/?format=geojson&timezone=Europe/Berlin&lang=fr
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="")
  for(i in 1:nrow(les_url) ) {
    url <- les_url$url[i]
    fic <- str_match(url, "/dataset/(.*)/download/")[2]
    carp("fic: %s", fic)
    file <- sprintf("%s/%s.geojson", dl_dir, fic)
    url <- sprintf("%s&epsg=4326", url)
    if( file.exists(file) && force == FALSE) {
      next
    }
    download.file(url, file, quiet = FALSE, mode = "wb")
    last
  }
}
opendatarennes_lire_couche_sf <- function(couche, force=FALSE) {
  library(sf)
  library(tidyverse)
  dl_dir <- sprintf("%s/bvi35/CouchesRennesAtlas/opendatarennes", Drive)
  dl_dir <- sprintf("%s/bvi35/CouchesRennesMetropole", Drive)
  dsn <- sprintf("%s/%s.geojson", dl_dir, couche)
  carp("dsn: %s", dsn)
  sf <- st_read(dsn)
  sf <- sf %>%
    st_transform(., "+init=epsg:2154")
  return(sf)
}