# <!-- coding: utf-8 -->
#
# quelques fonctions géographiques
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# https://rpubs.com/nabilabd/118172
#
# lecture d'un fichier ogr
ogr_lire <- function(dsn) {
  require(rgdal)
  require(rgeos)
  carp("%s", dsn)
  layer <- ogrListLayers(dsn)
  carp("layers: %s %s", layer, dsn)
# , verbose = FALSE
  dfSP <- readOGR(dsn, layer=layer, stringsAsFactors=FALSE, use_iconv=TRUE, encoding="UTF-8")
  carp("CRS: %s",proj4string(dfSP))
  dfSP <- spTransform(dfSP, CRS("+init=epsg:2154"))
  return(dfSP)
}
# écriture d'un fichier ogr
ogr_ecrire <- function(spdf, dsn, layer, driver="ESRI Shapefile", crs=FALSE) {
  require(rgdal)
  Log(sprintf("ogr_ecrire() %s", dsn))
  if ( crs != FALSE) {
   spdf <- spTransform(spdf, CRS(crs))
  }
  nom <- dsn
  nom <- gsub("\\..*$","", nom)
  nom <- gsub(".*/", "", nom)
  writeOGR(spdf, dsn, layer, driver=driver, overwrite_layer=TRUE)
}
extent2spdf <- function(e) {
  library(raster)
  sp <- as(e, 'SpatialPolygons')
  proj4string(sp) <- CRS("+init=epsg:2154")
  spdf <- SpatialPolygonsDataFrame(sp, data.frame(ID=1:length(sp)))
  return(spdf)
}
extent2sp <- function(e) {
  library(raster)
  sp <- as(e, 'SpatialPolygons')
  proj4string(sp) <- CRS("+init=epsg:2154")
  return(sp)
}
# https://stackoverflow.com/questions/43436466/create-grid-in-r-for-kriging-in-gstat
bb2grid <- function(bb) {
  v <- as.vector(bb)
  e <- extent(v[1], v[3], v[2], v[4])
  ras <- raster(e)
  cell_size <- 50
  res(ras) <- c(cell_size, cell_size)
  ras[] <- 0
  projection(ras) <- CRS("+init=epsg:2154")
#  plot(ras)
  st_grid <- rasterToPoints(ras, spatial = TRUE)
  gridded(st_grid) <- TRUE
  st_grid <- as(st_grid, "SpatialPixels")
  plot(st_grid)
}
#
# en direct de
# https://www.r-bloggers.com/great-circle-distance-calculations-in-r/
# Calculates the geodesic distance between two points specified by radian latitude/longitude using the
# Spherical Law of Cosines (slc)
gcd.slc <- function(long1, lat1, long2, lat2) {
  R <- 6371 # Earth mean radius [km]
  d <- acos(sin(lat1)*sin(lat2) + cos(lat1)*cos(lat2) * cos(long2-long1)) * R
  return(d) # Distance in km
}
# Calculates the geodesic distance between two points specified by radian latitude/longitude using the
# Haversine formula (hf)
gcd.hf <- function(long1, lat1, long2, lat2) {
  R <- 6371 # Earth mean radius [km]
  delta.long <- (long2 - long1)
  delta.lat <- (lat2 - lat1)
  a <- sin(delta.lat/2)^2 + cos(lat1) * cos(lat2) * sin(delta.long/2)^2
  c <- 2 * asin(min(1,sqrt(a)))
  d = R * c
  return(d) # Distance in km
}
# Calculates the geodesic distance between two points specified by radian latitude/longitude using
# Vincenty inverse formula for ellipsoids (vif)
gcd.vif <- function(long1, lat1, long2, lat2) {

  # WGS-84 ellipsoid parameters
  a <- 6378137         # length of major axis of the ellipsoid (radius at equator)
  b <- 6356752.314245  # ength of minor axis of the ellipsoid (radius at the poles)
  f <- 1/298.257223563 # flattening of the ellipsoid

  L <- long2-long1 # difference in longitude
  U1 <- atan((1-f) * tan(lat1)) # reduced latitude
  U2 <- atan((1-f) * tan(lat2)) # reduced latitude
  sinU1 <- sin(U1)
  cosU1 <- cos(U1)
  sinU2 <- sin(U2)
  cosU2 <- cos(U2)

  cosSqAlpha <- NULL
  sinSigma <- NULL
  cosSigma <- NULL
  cos2SigmaM <- NULL
  sigma <- NULL

  lambda <- L
  lambdaP <- 0
  iterLimit <- 100
  while (abs(lambda-lambdaP) > 1e-12 & iterLimit>0) {
    sinLambda <- sin(lambda)
    cosLambda <- cos(lambda)
    sinSigma <- sqrt( (cosU2*sinLambda) * (cosU2*sinLambda) +
                      (cosU1*sinU2-sinU1*cosU2*cosLambda) * (cosU1*sinU2-sinU1*cosU2*cosLambda) )
    if (sinSigma==0) return(0)  # Co-incident points
    cosSigma <- sinU1*sinU2 + cosU1*cosU2*cosLambda
    sigma <- atan2(sinSigma, cosSigma)
    sinAlpha <- cosU1 * cosU2 * sinLambda / sinSigma
    cosSqAlpha <- 1 - sinAlpha*sinAlpha
    cos2SigmaM <- cosSigma - 2*sinU1*sinU2/cosSqAlpha
    if (is.na(cos2SigmaM)) cos2SigmaM <- 0  # Equatorial line: cosSqAlpha=0
    C <- f/16*cosSqAlpha*(4+f*(4-3*cosSqAlpha))
    lambdaP <- lambda
    lambda <- L + (1-C) * f * sinAlpha *
              (sigma + C*sinSigma*(cos2SigmaM+C*cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)))
    iterLimit <- iterLimit - 1
  }
  if (iterLimit==0) return(NA)  # formula failed to converge
  uSq <- cosSqAlpha * (a*a - b*b) / (b*b)
  A <- 1 + uSq/16384*(4096+uSq*(-768+uSq*(320-175*uSq)))
  B <- uSq/1024 * (256+uSq*(-128+uSq*(74-47*uSq)))
  deltaSigma = B*sinSigma*(cos2SigmaM+B/4*(cosSigma*(-1+2*cos2SigmaM^2) -
                                      B/6*cos2SigmaM*(-3+4*sinSigma^2)*(-3+4*cos2SigmaM^2)))
  s <- b*A*(sigma-deltaSigma) / 1000

  return(s) # Distance in km
}
# Convert degrees to radians
deg2rad <- function(deg) return(deg*pi/180)
# Calculates the geodesic distance between two points specified by degrees (DD) latitude/longitude using
# Haversine formula (hf), Spherical Law of Cosines (slc) and Vincenty inverse formula for ellipsoids (vif)
gcd <- function(long1, lat1, long2, lat2) {
  # Convert degrees to radians
  long1 <- deg2rad(long1)
  lat1 <- deg2rad(lat1)
  long2 <- deg2rad(long2)
  lat2 <- deg2rad(lat2)

  return(list(haversine = gcd.hf(long1, lat1, long2, lat2),
                 sphere = gcd.slc(long1, lat1, long2, lat2),
               vincenty = gcd.vif(long1, lat1, long2, lat2)) )
}
# fusion d'un dataframe spatial avec un dataframe
# en direct de https://r-forge.r-project.org/scm/viewvc.php/pkg/R/carte.qual.R?view=markup&root=rgrs
# récupération de l'ordre initial
mergeSP <- function (sp, data, varname, sp.key="id", data.key="id") {
  require(sp)
  if ( DEBUG ) {
    print(sprintf("mergeSP() sp.key:%s", sp.key))
    print(summary(sp))
    print(sprintf("mergeSP() data.key:%s", data.key))
    print(head(data))
  }
  tmp <- data[,c(data.key, varname)]
# Création d'une variable temporaire pour récuperer l'ordre initial après le merge (Joel Gombin)
  sp@data$rgrs.temp.sort.var <- 1:nrow(sp@data)
  sp@data <- merge(sp@data, tmp, by.x=sp.key, by.y=data.key, all.x=TRUE, all.y=FALSE, sort=FALSE)
  sp@data <- sp@data[order(sp@data$rgrs.temp.sort.var, na.last = TRUE),]
  return(sp)
}
mergeSP_ <- function(sp, data, varname, sp.key="id", data.key="id") {
  require(sp)
  sp@data$rgrs.temp.sort.var <- 1:nrow(sp@data)
  sp@data <- merge(x=sp@data, y=data, by.x=sp.key, by.y=data.key, all.x=TRUE, all.y=FALSE, sort=FALSE)
  sp@data <- sp@data[order(sp@data$rgrs.temp.sort.var, na.last = TRUE),]
  return(sp)
}
#
# le crop d'un spatial dataframe par un polygone
# http://stackoverflow.com/questions/13982773/crop-for-spatialpolygonsdataframe
# http://qcbs.ca/wiki/_media/gisonr.pdf page 68
# http://geostat-course.org/system/files/monday_slides_0.pdf
# http://www.bias-project.org.uk/ASDARcourse/unit1_slides.pdf
cropSP <- function(dfSP, clipSP) {
  require(rgeos)
  gI <- gIntersects(dfSP, clipSP, byid=TRUE)
# create the new spatial polygons object.
  inter <- vector(mode="list", length=length(which(gI)))
  j <- 1
  df <- as.data.frame(matrix(nrow=length(which(gI)), ncol=ncol(dfSP@data)), stringsAsFactors=FALSE)
  colnames(df) <- colnames(dfSP@data)
  print(summary(df))
  print(sprintf("cropSP() nrow:%s ncol:%s", length(which(gI)), ncol(dfSP@data) ))
  for (i in seq(along=gI)) if (gI[i]) {
#  	print(sprintf("cropSP() i:%s", i))
    inter[[j]] <- gIntersection(dfSP[i,], clipSP)
    if ( class(inter[[j]]) == 'SpatialPoints' ) {
      next;
    }
    if ( DEBUG ) {
      print(sprintf("cropSP() i:%s j:%s class:%s",  i, j, class(inter[[j]])))
    }
#    print(head(inter[[j]]))
    inter[[j]] <- spChFIDs(inter[[j]], as.character(j))
    df[j,] <- dfSP@data[i,]
    j <- j+1
  }
  rownames(df) = 1:nrow(df)
  if ( DEBUG ) {
    print(head(df))
    print(row.names(df))
  }
# use rbind.SpatialPolygons method to combine into a new object.
  inter1 <- do.call("rbind", inter)
  print(sprintf("cropSP()  class(inter1): %s", class(inter1)))
  if ( class(inter1) == 'SpatialPolygons' ) {
    spdf <- SpatialPolygonsDataFrame(inter1, data=df)
  }
  if ( class(inter1) == 'SpatialLines' ) {
    spdf <- SpatialLinesDataFrame(inter1, data=df)
  }
  if ( DEBUG ) {
    print(sprintf("cropSP() fin"))
  }
  return(spdf)
}
cropSP2 <- function(spdf, crop.spdf) {
  require(rgeos)
  print(str(spdf))
  spdf$id <- seq(1, nrow(spdf), 1)
  print(sprintf("cropSP2()"))
  over <- rgeos::gIntersects(spdf, crop.spdf, byid = TRUE)
  spdf <- spdf[over[spdf$id] > 0,]
  return(spdf)
}
clip.poly <- function(spdf, clip, Id) {
  nb <- length(clip@polygons )
  k <- 0
  COMB <- list()
  # Decoupage par polygone du clip
  for (i in 1:nb) {
  # ieme polygone du clip
    xx <- clip[i, ]
  # Jointure spatiale
    pos <- which(!is.na(over(spdf, xx)))
  # Extraction par polygone de spdf superpose
    states <- list()
    for (j in 1:length(pos)) states[[j]] <- spdf[pos[j], ]
  # Intersection
      inters <- list()
    for (j in 1:length(pos)) {
      inters[[j]] <- gIntersection(states[[j]], xx)
    }
  # Identifiants uniques
    for (j in 1:length(pos)) {
      inters[[j]] <- spChFIDs(inters[[j]], as.character(k))
      k <- k + 1
    }
  # Combinaison des polygones de spdf decoupes
    comb <- inters[[1]]
    for (j in 2:length(pos)) comb <- spRbind(comb, inters[[j]])
  # Rajout de la table d'attributs
    mat <- spdf@data [pos, ]
    z <- sapply(comb@polygons , function(x) slot(x, "ID"))
    rownames(mat) <- z
    comb <- SpatialPolygonsDataFrame(comb, mat)
  # Placement temporaire dans la liste
    COMB[[i]] <- comb
  }
  # Combinaison de tous les decoupages
  CLIP <- COMB[[1]]
  if (nb > 1) {
    for (j in 2:length(COMB)) CLIP <- spRbind(CLIP, COMB[[j]])
  }
  # Dissolution par id
  temp <- gUnionCascaded(CLIP, id = as.character(CLIP@data [, Id]))
  # Rajout d'une table d'attributs
  id <- sapply(temp@polygons , function(x) slot(x, "ID"))
  mat <- data.frame()
  for (k in 1:length(id)) {
    pos <- which(CLIP$Code == id[k])
    mat <- rbind(mat, CLIP@data [pos[1], ])
  }
  rownames(mat) <- id
  CLIP <- SpatialPolygonsDataFrame(temp, mat)
  # Retour de la fonction
  return(CLIP)
}
emprise_v0 <- function(spdf) {
  boundingBox <- bbox(spdf)
  print(sprintf("emprise() X: %s => %s", boundingBox[1,1], boundingBox[1,2]))
  print(sprintf("emprise() Y: %s => %s", boundingBox[2,1], boundingBox[2,2]))
}
emprise <- function(spdf) {
  boundingBox <- bbox(spdf)
  carp(" X: %s => %s Y: %s => %s", boundingBox[1,1], boundingBox[1,2], boundingBox[2,1], boundingBox[2,2])
}
#
# découpe d'une ligne
# http://rstudio-pubs-static.s3.amazonaws.com/10685_1f7266d60db7432486517a111c76ac8b.html
#
# calcul de la boite englobante en format 4/3
englobante <- function(emprise, xMin=100,  yMin = 100, marge=100, ratio = 4/3, grille = 0) {
  xmin <- emprise[1]
  ymin <- emprise[2]
  xmax <- emprise[3]
  ymax <- emprise[4]
#  print(sprintf("englobante() llur: %.0f,%.0f,%.0f,%.0f", xmin, ymin, xmax, ymax))
  xmin <- xmin - marge
  ymin <- ymin - marge
  xmax <- xmax + marge
  ymax <- ymax + marge
# au moins 600 mètres de hauteur
  if ( ( ymax - ymin ) < yMin ) {
    delta = ( yMin - ( ymax - ymin ) ) / 2
    ymin = ymin - delta
    ymax = ymax + delta
  }
# au moins 600 mètres de largeur
  if ( ( xmax - xmin ) < xMin ) {
    delta = ( xMin - ( xmax - xmin ) ) / 2
    xmin = xmin - delta
    xmax = xmax + delta
  }
  Ratio = ( xmax - xmin ) / ( ymax - ymin )
#  print(sprintf("englobante() ullr: xmin:%.0f ymax:%.0f xmax:%.0f ymin:%.0f %.1f", xmin, ymax, xmax, ymin,Ratio))
  if ( ratio > 0 ) {
    if ( Ratio > ratio ) {
      print(sprintf("englobante() sup ratio: %.1f %.1f", ratio,Ratio))
  } else {
# il faut augmenter sur les x
      print(sprintf("englobante() inf ratio: %.1f %.1f", ratio,Ratio))
      delta = ( ymax - ymin ) * ratio - ( xmax - xmin)
      xmax = xmax + delta / 2
      xmin = xmin - delta / 2
    }
  }
  Ratio = ( xmax - xmin ) / ( ymax - ymin )
#  print(sprintf("englobante() ullr: %.0f %.0f %.0f %.0f %.1f", xmin, ymax, xmax, ymin,Ratio))
  if ( grille == 1 ) {
#    print(sprintf("englobante() grille:%s marge:%.0f", grille, marge))
    xmin <- floor(xmin/marge) * marge
    ymin <- floor(ymin/marge) * marge
    xmax <- ceiling(xmax/marge) * marge
    ymax <- ceiling(ymax/marge) * marge
  }
  if ( grille == 2 ) {
#    print(sprintf("englobante() grille:%s marge:%.0f", grille, marge))
    xmin <- xmin - marge
    ymin <- ymin - marge
    xmax <- xmax + marge
    ymax <- ymax + marge
  }
  Ratio = ( xmax - xmin ) / ( ymax - ymin )
  print(sprintf("englobante() ullr: %.0f %.0f %.0f %.0f %.1f", xmin, ymax, xmax, ymin, Ratio))
  emprise[1] <- xmin
  emprise[2] <- ymin
  emprise[3] <- xmax
  emprise[4] <- ymax
  return(emprise)
}
#
geo_emprise <- function(spdf, pas=100) {
  carp()
  e <- as.vector(bbox(spdf))
  e <- round(e)
  w <- e[3] - e[1]
  pas <- round(0.05*w)
  e[1] <- floor(e[1]/pas) * pas
  e[2] <- floor(e[2]/pas) * pas
  e[3] <- ceiling(e[3]/pas) * pas
  e[4] <- ceiling(e[4]/pas) * pas
  Log(sprintf("fonds_emprise() w: %s pas: %s e: %s %s %s %s", w, pas, e[1], e[2], e[3], e[4]))
  return(e)
}
geo_emprise_wgs84 <- function(spdf, pas=100) {
  carp()
  library(raster)
  e <- geo_emprise(spdf, pas)
  sp <- as(extent(e[1], e[3], e[2], e[4]), 'SpatialPolygons')
  proj4string(sp) <- CRS("+init=epsg:2154")
  sp <- spTransform(sp, CRS("+init=epsg:4326"))
  e <- as.vector(bbox(sp))
  Log(sprintf("fonds_emprise_wgs84() e: %s %s %s %s", e[1], e[2], e[3], e[4]))
  return(e)
}
# http://journal.r-project.org/archive/2011-1/RJournal_2011-1_Murrell.pdf
#
# lecture d'un fichier raster
rasterLec <- function(rasterFic, outputFic = "", maxW = 0 ) {
  print(sprintf("rasterLec() ___ %s",rasterFic))
  library(raster)
  library(rgdal)
  library(foreign)
  library(maptools)
  img <-  brick(rasterFic)
#  print(img)
  w <- img@ncols
  h <- img@nrows
  if ( maxW > 0 ) {
    if ( w > maxW ) {
      print(sprintf("rasterLec() >2048 w:%.0f h:%.0f",w,h))
      ratio <- w /maxW
      w <- ceiling(w / ratio)
      h <- ceiling(h / ratio)
    }
  }
  filext <-  sub("^.*[.]", "", outputFic, perl=TRUE)
  print(sprintf("rasterLec() w:%.0f h:%.0f filext:%s", w, h, filext))
  if ( filext == 'jpg' ) {
    jpeg(outputFic,width = w, height = h, units = "px", pointsize = 12, bg="white")
  } else if ( filext == 'png' ) {
    png(outputFic,width = w, height = h, units = "px", pointsize = 12, bg="white")
  } else {
    dev.new( width = w, height = h, units = "px", pointsize = 12)
  }
  plotRGB(img,axes=FALSE, maxpixels=30000000)
  return(img)
}
#
# lecture d'un fichier raster avec extraction d'une zone
rasterLecEmprise <- function(rasterFic,xmin, ymax, xmax, ymin,  outputFic = "", maxW = 0) {
  library(raster)
  library(rgdal)
  library(foreign)
  library(maptools)
  imgFic <-  brick(rasterFic)
  print(imgFic)
  print(sprintf("rasterLecEmprise() ullr: %.0f %.0f %.0f %.0f lh: %.0fx%.0f", xmin, ymax, xmax, ymin,xmax-xmin,ymax-ymin))
  img <- crop(imgFic,extent(xmin, xmax, ymin, ymax))
#  img <- imgFic
#  show(img)
  printRGB(img, outputFic, maxW)
  print(sprintf("rasterLecEmprise() outputFic : %s", outputFic))
  return(img)
}
printRGB <- function(img, outputFic = "", maxW = 0  ) {
  w <<- img@ncols
  h <<- img@nrows
  if ( maxW > 0 ) {
    if ( w > maxW ) {
      print(sprintf("printRGB() >2048 w:%.0f h:%.0f",w,h))
      ratio <- w /maxW
      w <<- ceiling(w / ratio)
      h <<- ceiling(h / ratio)
    }
  }
  filext <-  sub("^.*[.]", "", outputFic, perl=TRUE)
  print(sprintf("printRGB() w:%.0f h:%.0f outputFic:%s filext:%s", w, h, outputFic, filext))
  if ( filext == 'jpg' ) {
    jpeg(outputFic,width = w, height = h, units = "px", pointsize = 12, bg="white")
  } else if ( filext == 'png' ) {
    png(outputFic,width = w, height = h, units = "px", pointsize = 12, bg="white")
  } else {
    dev.new( width = w, height = h, units = "px", pointsize = 12)
  }
  plotRGB(img,axes=FALSE, maxpixels=30000000)
}
#
# mise en place de l'échelle
echelle_v1 <- function() {
  lg <- 2000
  arrows(
    par()$usr[1] + 1000,
    par()$usr[3] + 1000,
    par()$usr[1] + 1000 + lg,
    par()$usr[3] + 1000,
    lwd = 2,
    code = 3,
    col = "red",
    angle = 90,
    length = 0.05
  )
  text(
    par()$usr[1] + 1200,
    par()$usr[3] + 1300,
    "2 km",
    pos = 4,
    col = "red",
    cex = 2
  )
}
geo_copyright <- function(texte, deltaX=25, deltaY=25) {
  largeur <- par()$usr[2]-par()$usr[1]
  hauteur <- par()$usr[4]-par()$usr[3]
  l <- round(largeur/deltaX,0)
  h <- round(hauteur/deltaY,0)
  Encoding(texte) <- 'UTF-8'
  text(
    par()$usr[2] - l,
    par()$usr[3] + h,
    texte,
    adj=c(1, .5),
    font=2,
    col = "black",
#    cex = cex
  )
}
#
#
geo_legende <- function(texte = 'toto', pos = "topleft", deltaX = 25, deltaY = 25) {
  x1 <- par()$usr[1]
  x2 <- par()$usr[2]
  y1 <- par()$usr[3]
  y2 <- par()$usr[4]
  largeur <- x2 - x1
  hauteur <- y2 - y1
  l <- round(largeur/deltaX,0)
  h <- round(hauteur/deltaY,0)
#  carp('largeur:%s hauteur:%s l:%s h:%s', largeur, hauteur, l, h)
  positions <- c("topleft", "bottomleft", "bottomright", "topright")
  if(!pos %in% positions) {
    stop('*** pos')
  }
  adj_X <- 0
  adj_Y <- .5
  if ( pos == "topleft" ) {
    X1 <- x1 + l
    Y1 <- y2 - h
  }
  if ( pos == "bottomleft" ) {
    X1 <- x1 + l
    Y1 <- y1 + h
  }
  if ( pos == "bottomright" ) {
    X1 <- x2 - l
    Y1 <- y1 + h
  }
  if ( pos == "topright" ) {
    X1 <- x2 - l
    Y1 <- y2 - h
    adj_X <- 1
    adj_Y <- 1
  }
  Encoding(texte) <- 'UTF-8'
  texte_hauteur <- strheight(texte)
  text(
    X1,
    Y1,
    texte,
    adj = c(adj_X, adj_Y),
    font = 2,
    col = "black",
#    cex = cex
  )
}
#
# v2 : détermination de l'unité
geo_echelle <- function(pos = "bottomleft", deltaX=25, deltaY=25) {
  x1 <- par()$usr[1]
  x2 <- par()$usr[2]
  y1 <- par()$usr[3]
  y2 <- par()$usr[4]
  largeur <- x2 - x1
  hauteur <- y2 - y1
  l <- round(largeur/deltaX,0)
  h <- round(hauteur/deltaY,0)
#  carp('largeur:%s hauteur:%s l:%s h:%s', largeur, hauteur, l, h)
  echelle <- largeur / 7
  echelles <- c(100, 200, 500, 1000, 2000, 5000)
  i <- which(abs(echelles-echelle)==min(abs(echelles-echelle)))
  lgText <- c("100 mètres", "200 mètres", "500 mètres", "1 km", "2 km", "5 km")[i]
#  Encoding(lgText) <- 'UTF-8'
  lg <- echelles[i]
  positions <- c("bottomleft", "bottomright")
  if(!pos %in% positions) {
    stop('*** pos')
  }
  if ( pos == "bottomleft" ) {
    X1 <- x1 + l
    Y1 <- y1 + h
  }
  if ( pos == "bottomright" ) {
    X1 <- x2 - l - lg
    Y1 <- y1 + h
  }
  arrows(
    X1,
    Y1,
    X1 + lg,
    Y1,
    lwd = 4,
    code = 3,
    col = "black",
    angle = 90,
    length = 0.05
  )
  cex <- round(largeur / 300, 1)
  texte_hauteur <- strheight(lgText)
  text(
    X1 + lg/2,
    Y1 - texte_hauteur,
    lgText,
    adj=c(.5, .5),
    font=2,
    col = "black",
#    cex = cex
  )
}
# http://www.jstatsoft.org/v19/c01/paper
scalebar2 <- function(loc, length, unit="km",division.cex=.8) {
  if(missing(loc)) stop("loc is missing")
  if(missing(length)) stop("length is missing")
  x <- c(0,length/c(4,2,4/3,1),length*1.1)+loc[1]
  y <- c(0,length/(10*3:1))+loc[2]
  cols <- rep(c("black","white"),2)
  for (i in 1:4) rect(x[i],y[1],x[i+1],y[2],col=cols[i])
  for (i in 1:5) segments(x[i],y[2],x[i],y[3])
  labels <- x[c(1,3)]-loc[1]
  labels <- append(labels,paste(x[5]-loc[1],unit))
  text(x[c(1,3,5)],y[4],labels=labels,adj=.5,cex=division.cex)
}
#
# calcul des valeurs d'un raster type mnt
geo_raster2values <- function(img) {
  v <- getValues(img)
  c <- class(v)
  if ( c == "integer") {
#    Carp("class: %s", c)
    t <- tibble::enframe(v, name = NULL)
  } else {
    t <- as_tibble(v)
  }
  colnames(t) <- c("code")
  df <- t %>%
    group_by(code) %>%
    summarize(nb = n()) %>%
    arrange(nb) %>%
    na.omit()
  return(df)
}
#
# lecture d'un fichier kml avec des ExtendedData
# https://github.com/r-spatial/sf/issues/499
geo_readKML<-function(file,...) {
  library(xml2)
  sp_obj <- st_read(file,...)
  xml1 <- read_xml(file)
# extract name and type of variables
  variable_names <- xml_find_first(xml1, ".//d1:Schema[@name]") %>%
    xml2::xml_contents() %>%
    xml2::xml_attrs() %>%
    do.call(rbind,.) %>%
    glimpse()

  # return sp_obj if no ExtendedData is present
  if (is.null(variable_names)) return(sp_obj)

  data<-lapply(seq(nrow(variable_names)),function(x) {
    x1 <- xml_find_all(xml1,paste0(".//d1:SimpleData[@name=\"",variable_names[x,1],"\"]")) %>%
      xml_contents()
    if (variable_names[x,2]=="string") return(xml_text(x1))
    xml_double(x1)
  }) %>%
    do.call(cbind,.) %>%
    as.data.frame()

  colnames(data)<-variable_names[,1]
  sp_obj[,variable_names[,1]] <- data
  sp_obj
}
# https://mitchellgritts.com/posts/load-kml-and-kmz-files-into-r/
geo_readKMZ<-function(dsn,...) {
  target_file <- sprintf("%s.zip", dsn)
  target_dir <- dirname(dsn)
  fs::file_copy(dsn, target_file, overwrite = TRUE)
  unzip(target_file, overwrite = TRUE, exdir = target_dir )
  dsn <- sprintf("%s/doc.kml", target_dir)
  carp('dsn: %s', dsn)
  nc <- st_read(dsn)
  return(invisible(nc))
}