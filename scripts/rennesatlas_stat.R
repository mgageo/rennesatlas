# <!-- coding: utf-8 -->
#
# quelques fonction pour l'atlas Rennes
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# les stat
#
# un histogramme
stat_histo_espece <- function(df0, a="nb_oiseaux") {
  library(ggplot2)
  library(dplyr)
  carp("nrow: %s", nrow(df0))
  df1 <- df0 %>%
    group_by(espece) %>%
    summarize(nb_donnees=n(), nb_oiseaux=sum(nombre))
#  View(df1);stop("***")

  df <- df1 %>%
    mutate(nb = get(a))
  df$espece <- factor(df$espece, levels = df$espece[order(df$nb)])
#  View(df)
  m <- max(df$nb)
  pas <- ceiling(max(df$nb)/4)
  carp("m: %s pas: %s", m, pas)
  les_pas <- c(0, pas, pas*2, pas*4)
#  View(les_pas)
  df$cut <- cut(df$nb, les_pas, include.lowest = TRUE, right = FALSE)
  theme.blank <- theme(
    axis.title.x = element_blank(),
    axis.title.y = element_blank(),
  )

  gg <- ggplot(data=df, aes(x=espece, y=nb, fill=cut)) +
    geom_bar(stat="identity", width=0.7) +
    scale_y_continuous( limits = c(0, pas*4) ) +
    theme.blank +
    scale_fill_manual(values=c("#e75e00", "#009e73", "#56b4e9")) +
    theme(legend.position = c(0.8, 0.2)) +
    theme(legend.title = element_blank()) +
#     annotate("text", x = c(2), y = c(60), label = nbj , color="black", size=5, fontface="bold") +
    coord_flip()
  print(gg)
  ggpdf(a)
}
