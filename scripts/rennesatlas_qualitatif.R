# <!-- coding: utf-8 -->
#
# quelques fonction pour Atlas de Rennes
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
# la partie qualitative
# source("geo/scripts/rennesatlas.R");qualitatif_especes_mailles()
qualitatif_especes_mailles <- function(force=FALSE) {
  library(tidyverse)
  carp()
  df <- points_sessions_lire(force=force) %>%
    glimpse()
  df1 <- df %>%
    group_by(espece, maille, point) %>%
    summarize(nb=n()) %>%
    group_by(espece, maille) %>%
    summarize(nb=n()) %>%
    filter(nb>2) %>%
    group_by(espece) %>%
    summarize(nb=n()) %>%
    filter(nb>40) %>%
    glimpse()
  df1 <- df %>%
    group_by(espece, maille, session) %>%
    summarize(nb=n()) %>%
    group_by(espece, maille) %>%
    summarize(nb=n()) %>%
    filter(nb>2) %>%
    glimpse() %>%
    group_by(espece) %>%
    summarize(nb=n()) %>%
    glimpse() %>%
    filter(nb>40) %>%
    glimpse()
}
