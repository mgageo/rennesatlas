# <!-- coding: utf-8 -->
#
# fonctions d'analyse du code
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# source("geo/scripts/misc_code.R"); code_lignes("geo/scripts/ika.R")
code <- function() {
  code_lignes("geo/scripts/rennesatlas.R")
}

#
# production d'un fichier pour mise à jour du dépot git
code_git <- function(fic, dest='c:/msys64/tmp/git.lst') {
  s <- readLines(fic)
  f <- gsub('geo/', '', fic)
  fichiers <- s[grepl('^\\s*source\\(', s)]
  fichiers <- sub('.*?"', '', fichiers)
  fichiers <- sub('".*$', '', fichiers)
  fichiers <- fichiers[! grepl('/mga', fichiers)]
  fichiers <- sub('^geo/', '', fichiers)
  fichiers <- c(f, fichiers)
#  print(fichiers)
  out <- file(dest, "wb")
  write(fichiers, file = out, append = TRUE)
}
#
# comptabilisation du nombre de lignes d'un projet
code_lignes <- function(fic, reg=".*") {
  carp("fic : %s", fic)
  library(R.utils)
  library(stringr)
  countLines(fic)
  s <- readLines(fic)
  l <- length(s)
  carp("fic : %s nb : %s", fic, l)
  fichiers <- vector()
  df <- as.data.frame(s)
  regex <- sprintf('^\\s*source\\("%s', reg)
  df <- subset(df, grepl(regex, df$s))
  df <- subset(df, ! grepl('/misc', df$s))
  r <- "(geo.*\\.R)"
  nb <- 0
  l <- 0
  for(i in 1:nrow(df) ) {
    f <- df[i, "s"]
    f <- str_match(f, r)[1]
#    carp(" f : %s", f)
    l <- countLines(f)
    carp("f : %s nb : %s", f, l)
    fichiers <- append(fichiers, f)
    nb <- nb + l
  }
  carp("total : %s nb jours : %s", nb, as.integer(nb / 200) )
  fichiers <- sub('^geo/', '', fichiers)
  fichiers <- fichiers[! grepl('/mga', fichiers)]
  print(fichiers)
}
#
# style d'un fichier
style <- function(fic) {
  carp("fic : %s", fic)
  library(lintr)
  lintr::lint(fic)
}