# <!-- coding: utf-8 -->
#
# quelques fonction pour l'atlas Rennes : l'atlas de Jo Le Lannic
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
#
# pour les limites de l'atlas et le nombre de mailles
# source("geo/scripts/rennesatlas.R");lannic_atlas()
lannic_atlas <- function(pas=1000) {
  carp()
  library(raster)
  couches_rasters_lire(force=FALSE)
  plotImg(imgGM13)
  rocade.spdf <- couches_rocade_lire()
  rennes.spdf <- couches_rennes_lire()
  extent.spdf <- couches_extent()
  plot(rocade.spdf, col = misc_col('yellow', 40),  lwd=3, add=TRUE)
  plot(rennes.spdf, border="green", lwd=3, add=TRUE)
  zone.spdf <- gUnion(rennes.spdf, rocade.spdf)
  grille.spdf <- grille_epsg(zone.spdf, pas, align=TRUE, offset=FALSE)
#
# la version maille pleine
  grille.spdf$id <- seq(1, nrow(grille.spdf), 1)
  over <- rgeos::gIntersects(grille.spdf, zone.spdf, byid = TRUE)
  spdf <- grille.spdf[over[grille.spdf$id] > 0,]
  spdf$NUMERO <- sprintf("%02d", seq(1, nrow(spdf), 1))
  plot(spdf, border = 'red', lwd=2, add=TRUE)
  text(coordinates(spdf), labels=spdf@data$NUMERO, font=2)
}
