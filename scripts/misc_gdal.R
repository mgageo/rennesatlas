# <!-- coding: utf-8 -->
#
# quelques fonctions d'utilisation/préparation des couches OGC
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
############################################################################################
#
#
# les librairies
# cp ./bin/gdal/apps/*.exe bin
# https://rdrr.io/rforge/gdalUtils/src/R/gdal_setInstallation.R
# avec qgis, ça marche
gdal_lib <- function() {
  if ( exists("gdal_lib.init") ) {
#    return()
  }
  carp()
  library(rgdal)
  library(gdalUtils)
#  return()
  gdalDir <- "C:/PROGRA~1/QGISWI~1/bin/"
  gdalDir <- sprintf("%s/gdal210/bin/", Drive)
  gdalDir <- sprintf("%s/gdal212/bin/", Drive)
  gdalDir <- sprintf("D:\\gdal231\\bin")
#  gdalDir <- sprintf("C:/OSGEO4~1/bin/")
  carp("gdalDir: %s", gdalDir)
  gdal_setInstallation(search_path = gdalDir, ignore.full_scan = TRUE, verbose = TRUE, rescan=FALSE)
#  gdal_setInstallation()
#  print(getOption("gdalUtils_gdalPath"))
  valid_install <- !is.null(getOption("gdalUtils_gdalPath"))
  if( ! valid_install) {
    stop("gdal_lib()")
  }
  gdal_lib.init <<- 1
#  stop()
}
# https://github.com/r-spatial/discuss/issues/31#issuecomment-643638775
#
gdal_rgdal <- function() {
  install::packages("remotes")
  remotes::install_github("rforge/rgdal/pkg")
  install.packages("gdalUtils", repos="http://R-Forge.R-project.org")
}
gdal_lib_v1 <- function() {
  library(rgdal)
  library(gdalUtils)
  gdalDir <- "C:/PROGRA~1/QGISWI~1/bin/"
  gdalDir <- "D:/gdal210/bin/"
  print(sprintf("gdal_lib() %s", gdalDir))
  gdal_setInstallation(search_path = gdalDir, ignore.full_scan = TRUE, verbose = TRUE)
#  gdal_setInstallation()
#  print(getOption("gdalUtils_gdalPath"))
  valid_install <- !is.null(getOption("gdalUtils_gdalPath"))
  if( ! valid_install) {
    stop("gdal_lib()")
  }
#  stop()
}
#
# blocage Windows
# https://docs.microsoft.com/fr-fr/windows/security/threat-protection/windows-defender-antivirus/configure-block-at-first-sight-windows-defender-antivirus
# pb gdal 3.0
# cf sdk, déplacement de fichiers
# http://www.gisinternals.com/query.html?content=filelist&file=release-1800-x64-gdal-mapserver.zip
gdal_cmd <-function(cmde) {
  cmde <- gsub('^.*ogr2ogr.exe"', 'ogr2ogr', cmde)
  cmde <- gsub('^.*gdal_translate.exe"', 'gdal_translate', cmde)
  cmd <- '@echo on
@setlocal enableextensions enabledelayedexpansion
SET SDK_ROOT=D:\\gdal240\\
SET "PATH=%SDK_ROOT%bin;%SDK_ROOT%bin\\gdal\\python\\osgeo;%SDK_ROOT%bin\\proj\\apps;%SDK_ROOT%bin\\gdal\\apps;%SDK_ROOT%bin\\ms\\apps;%SDK_ROOT%bin\\gdal\\csharp;%SDK_ROOT%bin\\ms\\csharp;%SDK_ROOT%bin\\curl;%PATH%"
SET "GDAL_DATA=%SDK_ROOT%bin\\gdal-data"
SET "GDAL_DRIVER_PATH=%SDK_ROOT%bin\\gdal\\plugins"
SET "PYTHONPATH=%SDK_ROOT%bin\\gdal\\python;%SDK_ROOT%bin\\ms\\python"
SET "PROJ_LIB=%SDK_ROOT%bin\\proj5\\SHARE"
'
  cmd <- paste(cmd, cmde)
  file <- sprintf("%s/rgdal.cmd", cfgDir)
  write(cmd, file = file, append = FALSE)
  carp('file: %s', file)
  message(cmd)
#  stop('!!!!!!!!!!!')
  cmd_output <- system(file, intern = TRUE)
  return(invisible(cmd_output))
}
#
# avec proj6
gdal_cmd6 <-function(cmde) {
  cmde <- gsub('^.*ogr2ogr.exe"', 'ogr2ogr', cmde)
  cmde <- gsub('^.*gdal_translate.exe"', 'gdal_translate', cmde)
  cmd <- '@echo on
@setlocal enableextensions enabledelayedexpansion
SET SDK_ROOT=D:\\gdal30\\
SET "PATH=%SDK_ROOT%bin;%SDK_ROOT%bin\\gdal\\python\\osgeo;%SDK_ROOT%bin\\proj\\apps;%SDK_ROOT%bin\\gdal\\apps;%SDK_ROOT%bin\\ms\\apps;%SDK_ROOT%bin\\gdal\\csharp;%SDK_ROOT%bin\\ms\\csharp;%SDK_ROOT%bin\\curl;%PATH%"
SET "GDAL_DATA=%SDK_ROOT%bin\\gdal-data"
SET "GDAL_DRIVER_PATH=%SDK_ROOT%bin\\gdal\\plugins"
SET "PYTHONPATH=%SDK_ROOT%bin\\gdal\\python;%SDK_ROOT%bin\\ms\\python"
SET "PROJ_LIB=%SDK_ROOT%bin\\proj6\\SHARE"
'
  cmd <- paste(cmd, cmde)
  file <- sprintf("%s/rgdal.cmd", cfgDir)
  write(cmd, file = file, append = FALSE)
  carp('file: %s', file)
  message(cmd)
#  stop('!!!!!!!!!!!')
  cmd_output <- system(file, intern = TRUE)
  return(invisible(cmd_output))
}
gdal_cmd_v0 <-function(cmde) {
  cmd_output <- system(file, intern = FALSE,
    ignore.stdout = FALSE, ignore.stderr = FALSE,
    wait = TRUE, input = NULL, show.output.on.console = TRUE,
    minimized = FALSE, invisible = FALSE
  )
  return(invisible(cmd_output))
}
gdal_utf <- function() {
  library(rgdal)
  library(stringi)
  Sys.getlocale("LC_CTYPE")
  sessionInfo()
  unlist(l10n_info())
  .Call("RGDAL_CPL_RECODE_ICONV", PACKAGE="rgdal")

  dsn <- 'D:/web.var/geo/COUCHES/opendatarennes/sous_quartiers_vdr_shp_lamb93/sous_quartiers_vdr.shp'
  layer <- 'sous_quartiers_vdr'

  setCPLConfigOption("SHAPE_ENCODING", "UTF-8")
  pt1 <- readOGR(dsn, layer, stringsAsFactors=FALSE)
  setCPLConfigOption("SHAPE_ENCODING", NULL)
  pt1$NOMSQUART[2]
  charToRaw(pt1$NOMSQUART[2])

  setCPLConfigOption("SHAPE_ENCODING", "")
  pt6 <- readOGR(dsn, layer, stringsAsFactors=FALSE)
  setCPLConfigOption("SHAPE_ENCODING", NULL)
  pt6$NOMSQUART[2]
  charToRaw(pt6$NOMSQUART[2])

  pt7 <- readOGR(dsn, layer, stringsAsFactors=FALSE, encoding="")
  gdal_utf_plot(pt7)

}
gdal_utf_plot <- function(spdf) {
  spdf$NOMSQUART[2]
  charToRaw(spdf$NOMSQUART[2])
  table(Encoding(spdf$NOMSQUART))
  stri_enc_mark(spdf$NOMSQUART)
  all(stri_enc_isutf8(pdf$NOMSQUART))
  spdf$NOMSQUART <- stri_encode(spdf$NOMSQUART, "", "UTF-8")
  stri_enc_mark(spdf$NOMSQUART)
  spdf$NOMSQUART <- iconv(spdf$NOMSQUART, "UTF-8")
  plot(spdf, border= "black", add=FALSE, lwd=3)
  text(coordinates(spdf), labels=spdf@data$NOMSQUART, cex=3, col="black")
}