# <!-- coding: utf-8 -->
#
# quelques fonction pour l'atlas Rennes
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
#
couches_rennes <- function() {
  carp()
  xmin <- 346000
  ymax <- 6800000
  xmax <- 360000
  ymin <- 6784000
  ncols <<- xmax - xmin
  nrows <<- ymax - ymin
}
#
# l'extent de la grille
couches_extent <- function(pas=500) {
  carp()
  library(raster)
  xmin <- 345000
  ymax <- 6795000
  xmax <- 358000
  ymin <- 6784000
  e <- extent(xmin-pas/2, xmax+pas/2, ymin-pas/2, ymax+pas/2)
  e <- extent(xmin, xmax, ymin, ymax)
  sp <- as(e, 'SpatialPolygons')
  proj4string(sp) <- CRS("+init=epsg:2154")
  spdf <- SpatialPolygonsDataFrame(sp, data.frame(ID=1:length(sp)))
  return(spdf)
}
#
# les couches ogc sur l'ensemble de l'atlas
# source("geo/scripts/rennesatlas.R");couches_atlas_get()
couches_atlas_get <- function(test=1) {
  carp()
  library(tidyverse)
  sf <- fonds_grille_lire_sf()
  for ( layer in c('pvci_fondplan', 'pvci_fondplan_nb', 'pvci_light_gris', 'pvci_nb') ) {
    ogc_url <- "https://public.sig.rennesmetropole.fr/geoserver/ows?"
    ogc_layer <- sprintf('ref_fonds:%s', layer)
    dsn <- sprintf("%s/Rennes_RM_%s_2154.tif", varDir, layer)
    if ( test == 1 | ! file.exists(dsn)) {
      img <- couches_atlas_get_layer(sf, dsn, ogc_url, ogc_layer)
    }
  }
}
#
# http://forums.cirad.fr/logiciel-R/viewtopic.php?t=9026
couches_atlas_get_layer <- function(sf, file, ogc_url, ogc_layer, marge=500) {
  carp()
  glimpse(sf)
#  stop('***')
  bb <- as.integer(as.vector(st_bbox(sf)))
  glimpse(bb)
  xmin <- bb[1] - marge
  ymin <- bb[2] - marge
  xmax <- bb[3] + marge
  ymax <- bb[4] + marge
# http://docs.geoserver.org/latest/en/user/services/wms/reference.html
  bbox <- sprintf("%s,%s,%s,%s", xmin, ymin, xmax, ymax)
  largeur <- xmax-xmin
  hauteur <- ymax-ymin
  carp("lxh:%sx%s", largeur, hauteur)
  wms_max <- 3072
  if ( largeur > wms_max ) {
    ratio <- largeur / wms_max
    largeur <- as.integer(largeur / ratio)
    hauteur <- as.integer(hauteur / ratio)
  }
  if ( hauteur > wms_max ) {
    ratio <- hauteur / wms_max
    largeur <- as.integer(largeur / ratio)
    hauteur <- as.integer(hauteur / ratio)
  }
  carp("lxh:%sx%s", largeur, hauteur)
  url <- sprintf("%sSERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&BBOX=%s&WIDTH=%s&HEIGHT=%s&SRS=EPSG:2154&LAYERS=%s&STYLES=&FORMAT=image/geotiff&DPI=96&TRANSPARENT=TRUE", ogc_url, bbox, largeur, hauteur, ogc_layer)
  carp("url: %s", url)
  url <- URLencode(url)
  options(timeout=6000)
  download.file(url, file, quiet = FALSE, mode = "wb")
  if ( interactive() ) {
#    img <- brick(file)
#    plotRGB(img)
  }
  if ( interactive() ) {
#    img <- raster(file)
#    plot(img)
  }
#  stop('***')
}
#
# source("geo/scripts/rennesatlas.R"); couches_carres_ecrire(test=1)
couches_carres_ecrire <- function(test=2) {
  carp()
  library(tidyverse)
  library(sf)
  sf <- fonds_grille_lire_sf() %>%
    glimpse()
  sf$id <- sf$NUMERO
  df <- st_set_geometry(sf, NULL)
  ogcDir <- sprintf("%s/ogc", varDir)
  dir.create(ogcDir, showWarnings = FALSE)
  n <- nrow(sf)
  fond <- 'gb_photo'
  fond <- 'ign_gm'
  fond <- 'rm_pvci'
  fond <- 'rm_ortho2017'
  for ( i in 1:n ) {
    if ( i > 4 ) {
#      break
    }
    sf1 <- sf[i,]
    parcours <- sf[[i, 'id']]
    carp("i: %s/%s parcours: %s", i, n, parcours)
    dsn <- sprintf("%s/%s_%s.tif", ogcDir, parcours, fond)
    if ( test == 1 | ! file.exists(dsn)) {
      img <- fonds_ogc_dl_bbox(sf1, dsn, fond, marge=200, size_max=1024)
    }
  }
}

#
# extraction des limites de Rennes à partir du fichier IGN
couches_rennes_ecrire <- function() {
  dsn <- sprintf('%s/ign/COMMUNE.shp', varDir)
  layer <- ogrListLayers(dsn)
  spdf <- readOGR(dsn, layer=layer, stringsAsFactors=FALSE, use_iconv=TRUE, encoding="UTF-8")
  spdf <- subset(spdf, spdf@data$INSEE_COM == '35238')
#  spdf <- spTransform(spdf, CRS("+init=epsg:2154"))
#  print(proj4string(spdf))
#  View(spdf@data)
  dsn <- sprintf('%s/rennes.shp', varDir)
  writeOGR(spdf, dsn, layer = 'donnees', driver="ESRI Shapefile", overwrite_layer=TRUE, layer_options= c(encoding= "UTF-8"))
  return(spdf)
}
couches_rennes_lire <- function() {
  dsn <- sprintf('%s/rennes.shp', varDir)
  layer <- ogrListLayers(dsn)
  spdf <- readOGR(dsn, layer=layer, stringsAsFactors=FALSE, use_iconv=TRUE, encoding="UTF-8")
#  proj4string(spdf) <- CRS("+init=epsg:2154")
  return(spdf)
}
#
# le tracé de la rocade avec boulevard des alliées fait sous https://maps.openrouteservice.org
couches_rocade_lire <- function() {
  dsn <- sprintf('%s/rocade_ba.geojson', varDir)
  spdf <- ogr_lire(dsn);
  return(spdf)
}
#
# les points stoc de Matthieu
couches_stoc_lire <- function() {
  dsn <- sprintf('%s/matthieu_stoc.geojson', varDir)
  spdf <- ogr_lire(dsn);
  spdf$id <- seq(1, nrow(spdf), 1)
  spdf <- geocode_reverse_spdf(spdf)
  return(invisible(spdf))
}
#
# passage des rasters à la bonne taille
couches_rasters <- function() {
  carp()
  library(sp)
  spdf <- couches_extent()
  les_dsn <- read.table(text="dsn
GP_GMGRIS_13_2154.tif
GP_GMGRIS_16_2154.tif
GP_PVA_16_2154.tif
GP_PVA_17_2154.tif
GB_PVA_16_2154.tif
RM_PVCI_14_2154.tif
RM_PVCI_17_2154.tif
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="")
  for(i in 1:nrow(les_dsn) ) {
    dsn <- sprintf("%s/%s", wmsDir, les_dsn$dsn[i])

    dest <- sprintf("%s/%s", varDir, les_dsn$dsn[i])
    carp(" dest: %s", dest)
    if ( ! file.exists(dsn) ) {
      carp(" dsn: %s", dsn)
      stop("***")
      next;
    }
    if ( file.exists(dest) ) {
#      next;
    }
    img <- brick(dsn)
    img <- crop(img, extent(spdf))
#    plotImg(img)
    rf <- writeRaster(img, filename=dest, format="GTiff", overwrite=TRUE)
  }
}
#
# lecture des fonds de carte raster
couches_rasters_lire <- function(force=FALSE) {
  carp()
  library(raster)
  if ( ! exists("imgGM")  | force==TRUE) {
    rasterFic <- sprintf("%s/GP_GMGRIS_16_2154.tif", varDir)
    carp(" rasterFic:%s", rasterFic)
    imgGM <<- brick(rasterFic)
  }
  if ( ! exists("imgGM13")  | force==TRUE) {
    rasterFic <- sprintf("%s/GP_GMGRIS_13_2154.tif", varDir)
    carp(" rasterFic:%s", rasterFic)
    imgGM13 <<- brick(rasterFic)
  }
  if ( ! exists("imgPVA")  | force==TRUE) {
    rasterFic <- sprintf("%s/Rennes_GP_PVA_17_2154.tif", varDir)
    rasterFic <- sprintf("%s/Rennes2017_GB_PVA_17_2154.tif", varDir)
#    rasterFic <- sprintf("%s/Rocade_GB_PVA_17_2154.tif", varDir)
    carp(" rasterFic:%s", rasterFic)
    imgPVA <<- brick(rasterFic)
  }
  if ( ! exists("imgPVCI")  | force==TRUE) {
#  D:\web.var\geoportail\wms\RennesAtlas\Rennes\Rennes_RM_PVCI_17_2154.tif
    rasterFic <- sprintf("%s/Rennes_RM_PVCI_17_2154.tif", varDir)
    carp(" rasterFic:%s", rasterFic)
    imgPVCI <<- brick(rasterFic)
  }
  if ( ! exists("imgPVCI14")  | force==TRUE) {
    rasterFic <- sprintf("%s/Rennes_RM_PVCI_14_2154.tif", varDir)
    carp(" rasterFic:%s", rasterFic)
    imgPVCI14 <<- brick(rasterFic)
  }
  if ( ! exists("imgOSM")  | force==TRUE) {
    rasterFic <- sprintf("%s/Rennes_GB_OSM_17_2154.tif", varDir)
    carp(" rasterFic:%s", rasterFic)
    imgOSM <<- brick(rasterFic)
  }
}
#
# http://maps.stamen.com/m2i/#terrain-background/2000:1000/15/48.1026/-1.7450
couches_stamen <- function(style="watercolor") {
  carp()
  require(OpenStreetMap)
  require(raster)
  zone.spdf <- fonds_grille_lire()
  zone.crs <- proj4string(zone.spdf)
  bb <- bbox(zone.spdf)
  bb[,1] <- bb[,1] - 500
  bb[,2] <- bb[,2] + 500
  e <- extent(c(bb[1,1], bb[1,2], bb[2,1], bb[2,2]))
  clip.sp <- as(e, "SpatialPolygons")
  proj4string(clip.sp) <- CRS(zone.crs)
  lonlat <- spTransform(clip.sp, CRS("+init=epsg:4326"))
  boundingBox <- bbox(lonlat)
  Log(sprintf("upper left %s %s lower right %s %s", boundingBox[2,2], boundingBox[1,1], boundingBox[2,1], boundingBox[1,2]))
  url <- sprintf("http://a.tile.stamen.com/%s/{z}/{x}/{y}.png", style)
  map <- openproj(openmap(c(boundingBox[2,2], boundingBox[1,1]), c(boundingBox[2,1], boundingBox[1,2]), 14, type=url, minNumTiles=40), projection = zone.crs)
  plot(map)
#  stop("***")
  r <- raster(map)
  r <- crop(r, e)
  plotRGB(r)
  plot(zone.spdf, add=TRUE)
  dsn <- sprintf("%s/ogc/stamen_%s.tif", varDir, style)
  rf <- writeRaster(r, filename=dsn, format="GTiff", overwrite=TRUE)
  carp(" dsn:%s", dsn)
}
couches_stamen_lire <- function(style="watercolor") {
  require(raster)
  dsn <- sprintf("%s/ogc/stamen_%s.tif", varDir, style)
  carp('dsn: %s', dsn)
  img <- brick(dsn)
  plotImg(img, maxpixels=500000)
}
couches_stamen_lire_img <- function(style="watercolor", force=FALSE) {
  if ( ! exists("imgSTAMEN")  | force==TRUE) {
    require(raster)
    dsn <- sprintf("%s/ogc/stamen_%s.tif", varDir, style)
    imgSTAMEN <<- brick(dsn)
  }
  return(invisible(imgSTAMEN))
}