# <!-- coding: utf-8 -->
#
# la partie carto analyse spatiale
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# la boundingbox de la zone d'étude
carto_bbox <- function(spdf) {
  carp()
  bb <- bbox(spdf)
  bb[,1] <- bb[,1] - 500
  bb[,2] <- bb[,2] + 500
  return(invisible(bb))
}
# https://www.r-bloggers.com/aggregating-spatial-points-by-clusters/
# http://r.789695.n4.nabble.com/Spatstat-Several-density-plots-using-the-same-scale-td870327.html
carto_groupes <- function(spdf, bb, trans=80) {
  library(spatstat)
  library(maptools)
  library(rgeos)
  carp("nrow: %s", nrow(spdf@data))
  sSp <- as(SpatialPoints(spdf), "ppp")  # convert points to pp class
  w <- owin(bb[1, ], bb[2,])
#  sSp <- sSp[w]
  sSp$window <- w
  Dens <- density(sSp, adjust = 0.2, diggle=TRUE)  # create density object
  class(Dens)  # just for interest: it's got it's of pixel image class
## [1] "im"
# https://stats.stackexchange.com/questions/26676/generating-visually-appealing-density-heat-maps-in-r
# https://www.r-bloggers.com/how-to-correctly-set-color-in-the-image-function/
  pal <- colorRampPalette(c("blue", "magenta", "red", "yellow", "white"), bias=2, space="Lab")
  pal_trans <- pal(5)
  pal <- colorRampPalette(c("blue", "magenta", "orange", "red", "darkred"), bias=2, space="Lab")
  pal_trans <- pal(5)
  pal_trans <- sprintf("%s%s", pal_trans, trans)
  pal_trans[1] <- "#FFFFFF00"
  plot(Dens, add=TRUE, col=pal_trans)
  return()
  contour(density(sSp, adjust = 0.2), nlevels = 5)  # plot as contours - this is where we're heading
  Dsg <- as(Dens, "SpatialGridDataFrame")  # convert to spatial grid class
  Dim <- as.image.SpatialGridDataFrame(Dsg)  # convert again to an image
  Dcl <- contourLines(Dim, nlevels = 4)  # create contour object - change 8 for more/fewer levels
  SLDF <- ContourLines2SLDF(Dcl, CRS(proj4string(spdf)))  # convert to SpatialLinesDataFrame
  plot(SLDF, col = terrain.colors(4))
  return()
  Polyclust <- gPolygonize(SLDF[5, ])
  gas <- gArea(Polyclust, byid = T)/10000
  Polyclust <- SpatialPolygonsDataFrame(Polyclust, data = data.frame(gas), match.ID = F)
  plot(Polyclust)
}
#
# ombrage du texte
#
# http://stackoverflow.com/questions/25631216/r-is-there-any-way-to-put-border-shadow-or-buffer-around-text-labels-en-r-plot
#
# ajout de ... pour prendre en compte les paramètres opyionnels
carto_shadowtext <- function(x, y=NULL, labels, col='white', bg='black',  theta= seq(0, 2*pi, length.out=50), r=0.2, ... ) {
  xy <- xy.coords(x,y)
  xo <- r*strwidth('A', ...)
  yo <- r*strheight('A', ...)
# draw background text with small shift in x and y in background colour
  for (i in theta) {
    text( xy$x + cos(i)*xo, xy$y + sin(i)*yo, labels, col=bg, ...)
  }
  # draw actual text in exact xy position in foreground colour
  text(xy$x, xy$y, labels, col=col, ... )
}
carto_text_spdf <- function(spdf, champ, col = "black", cex = 0.7, font=2, ...) {
  df <- coordinates(spdf)
  colnames(df) <- c("x","y")
  df <- cbind(df, spdf@data)
  carto_shadowtext(df$x, df$y, labels = df[,champ], cex=cex, col=col, font=2, ...)
}
carto_text_sf <- function(nc, champ, col = "white", cex = 0.7, font=2, ...) {
  carp()
  cc <- sf::st_coordinates(sf::st_centroid(x = sf::st_geometry(nc)))
  carto_shadowtext(x = cc[,1], y = cc[,2], labels = nc[[champ]], cex=cex, col=col, font=2, ...)
}
carto_boxtext_sf <- function(nc, champ, col = "white", cex = 0.7, font=2, ...) {
  carp()
  cc <- sf::st_coordinates(sf::st_centroid(x = sf::st_geometry(nc)))
  carto_boxtext(x = cc[,1], y = cc[,2], labels = nc[[champ]], cex=cex, font=2, ...)
}
carto_text <- function(nc, champ, col = "black", cex = 0.7, font=2, ...) {
  df <- st_coordinates(nc)
  colnames(df) <- c("x","y")
  df <- cbind(df, as.data.frame(nc)) %>%
    glimpse()
  carto_shadowtext(df$x, df$y, labels = df[,champ], cex=cex, font=font, ...)
}
' Add text with background box to a plot
#'
#' \code{boxtext} places a text given in the vector \code{labels}
#' onto a plot in the base graphics system and places a coloured box behind
#' it to make it stand out from the background.
#'
#' @param x numeric vector of x-coordinates where the text labels should be
#' written. If the length of \code{x} and \code{y} differs, the shorter one
#' is recycled.
#' @param y numeric vector of y-coordinates where the text labels should be
#' written.
#' @param labels a character vector specifying the text to be written.
#' @param col.text the colour of the text
#' @param col.bg color(s) to fill or shade the rectangle(s) with. The default
#' \code{NA} means do not fill, i.e., draw transparent rectangles.
#' @param border.bg color(s) for rectangle border(s). The default \code{NA}
#' omits borders.
#' @param adj one or two values in [0, 1] which specify the x (and optionally
#' y) adjustment of the labels.
#' @param pos a position specifier for the text. If specified this overrides
#' any adj value given. Values of 1, 2, 3 and 4, respectively indicate
#' positions below, to the left of, above and to the right of the specified
#' coordinates.
#' @param offset when \code{pos} is specified, this value gives the offset of
#' the label from the specified coordinate in fractions of a character width.
#' @param padding factor used for the padding of the box around
#' the text. Padding is specified in fractions of a character width. If a
#' vector of length two is specified then different factors are used for the
#' padding in x- and y-direction.
#' @param cex numeric character expansion factor; multiplied by
#' code{par("cex")} yields the final character size.
#' @param font the font to be used
#'
#' @return Returns the coordinates of the background rectangle(s). If
#' multiple labels are placed in a vactor then the coordinates are returned
#' as a matrix with columns corresponding to xleft, xright, ybottom, ytop.
#' If just one label is placed, the coordinates are returned as a vector.
#' @author Ian Kopacka
#' @examples
#' ## Create noisy background
#' plot(x = runif(1000), y = runif(1000), type = "p", pch = 16,
#' col = "#40404060")
#' boxtext(x = 0.5, y = 0.5, labels = "some Text", col.bg = "#b2f4f480",
#'     pos = 4, font = 2, cex = 1.3, padding = 1)
#' @export
carto_boxtext <- function(x, y, labels = NA, col.text = NULL, col.bg = NA,
        border.bg = NA, adj = NULL, pos = NULL, offset = 0.5,
        padding = c(0.5, 0.5), cex = 1, font = graphics::par('font')){

    ## The Character expansion factro to be used:
    theCex <- graphics::par('cex')*cex

    ## Is y provided:
    if (missing(y)) y <- x

    ## Recycle coords if necessary:
    if (length(x) != length(y)){
        lx <- length(x)
        ly <- length(y)
        if (lx > ly){
            y <- rep(y, ceiling(lx/ly))[1:lx]
        } else {
            x <- rep(x, ceiling(ly/lx))[1:ly]
        }
    }

    ## Width and height of text
    textHeight <- graphics::strheight(labels, cex = theCex, font = font)
    textWidth <- graphics::strwidth(labels, cex = theCex, font = font)

    ## Width of one character:
    charWidth <- graphics::strwidth("e", cex = theCex, font = font)

    ## Is 'adj' of length 1 or 2?
    if (!is.null(adj)){
        if (length(adj == 1)){
            adj <- c(adj[1], 0.5)
        }
    } else {
        adj <- c(0.5, 0.5)
    }

    ## Is 'pos' specified?
    if (!is.null(pos)){
        if (pos == 1){
            adj <- c(0.5, 1)
            offsetVec <- c(0, -offset*charWidth)
        } else if (pos == 2){
            adj <- c(1, 0.5)
            offsetVec <- c(-offset*charWidth, 0)
        } else if (pos == 3){
            adj <- c(0.5, 0)
            offsetVec <- c(0, offset*charWidth)
        } else if (pos == 4){
            adj <- c(0, 0.5)
            offsetVec <- c(offset*charWidth, 0)
        } else {
            stop('Invalid argument pos')
        }
    } else {
      offsetVec <- c(0, 0)
    }

    ## Padding for boxes:
    if (length(padding) == 1){
        padding <- c(padding[1], padding[1])
    }

    ## Midpoints for text:
    xMid <- x + (-adj[1] + 1/2)*textWidth + offsetVec[1]
    yMid <- y + (-adj[2] + 1/2)*textHeight + offsetVec[2]

    ## Draw rectangles:
    rectWidth <- textWidth + 2*padding[1]*charWidth
    rectHeight <- textHeight + 2*padding[2]*charWidth
    graphics::rect(xleft = xMid - rectWidth/2,
            ybottom = yMid - rectHeight/2,
            xright = xMid + rectWidth/2,
            ytop = yMid + rectHeight/2,
            col = col.bg, border = border.bg)

    ## Place the text:
    graphics::text(xMid, yMid, labels, col = col.text, cex = theCex, font = font,
            adj = c(0.5, 0.5))

    ## Return value:
    if (length(xMid) == 1){
        invisible(c(xMid - rectWidth/2, xMid + rectWidth/2, yMid - rectHeight/2,
                        yMid + rectHeight/2))
    } else {
        invisible(cbind(xMid - rectWidth/2, xMid + rectWidth/2, yMid - rectHeight/2,
                        yMid + rectHeight/2))
    }
}