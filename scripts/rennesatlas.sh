#!/bin/sh
# <!-- coding: utf-8 -->
#T Rennes : atlas ornitho
# auteur: Marc Gauthier
[ -f ../win32/scripts/misc.sh ] && . ../win32/scripts/misc.sh
[ -f ../win32/scripts/mga.sh ] && . ../win32/scripts/mga.sh
#f CONF:
CONF() {
  LOG "CONF debut"
  CFG="RENNESATLAS"
  [ -d "${CFG}" ] || mkdir "${CFG}"
  varDir="/d/bvi35/CouchesRennesAtlas"
  webDir="/d/web.heb/bv/rennesatlas"
  LOG "CONF fin"
}
#f JSON:
JSON() {
  LOG "JSON debut"
  cp -pv ${varDir}/grille_lambert_1000.json ${webDir}/MAILLES/grille_lambert_1000.geojson
  cp -pv ${varDir}/points_lambert_1000.json ${webDir}/POINTS/points_lambert_1000.geojson
  cp -pv ${varDir}/cercles_lambert_1000.json ${webDir}/POINTS/cercles_lambert_1000.geojson
  LOG "JSON fin"
}
#f leaflet:
leaflet() {
  LOG "leaflet debut"
  wget -O /d/web/leaflet/exemples/ipa_mailles.geojson 'http://ao35.free.fr/rennesatlas/rennesatlas.php?action=get_mailles'
  wget -O /d/web/leaflet/exemples/ipa_points.geojson 'http://ao35.free.fr/rennesatlas/rennesatlas.php?action=get_points'
  LOG "leaflet fin"
}
#F e: edition des principaux fichiers
e() {
  LOG "e debut"
  E scripts/rennesatlas.sh
  for f in scripts/rennesatlas*.R ; do
    E $f
  done
  LOG "e fin"
}
#F e_web: edition des principaux fichiers serveur
e_web() {
  LOG "e_web debut"
  ncd rennesatlas
  for f in rennesatlas.php ../inc/rennesatlas.php ; do
    E $f
  done
  LOG "e_web fin"
}
#f DOCX2TEX:
DOCX2TEX() {
  LOG "DOCX2TEX debut"
  _ENV_pandoc
  texDir=${CFG}/
  cd $texDir
  for md in *.docx; do
    tex=`basename "$md" .docxt`
    pandoc --verbose-docx --extract-media . -f docx $md -o $tex.tex
    cat $tex.tex
  done
  LOG "DOCX2TEX fin"
}
#
# le fil rouge de Matthieu
#f FILROUGE:
FILROUGE() {
  LOG "FILROUGE debut"
  _ENV_gdald
  f=fil_rouge_2
  ogr2ogr -f "GeoJSON" -lco COORDINATE_PRECISION=5 ${CFG}/filrouge/${f}.geojson ${CFG}/filrouge/${f}.kml
  LOG "FILROUGE fin"
}
#
# les points d'écoute
#F OBSP_clean:
OBSP_clean() {
  LOG "OBSP_clean debut"
  ncd rennesatlas
  cd POINTOBS
  jourDir=$(date +%Y%m%d)
  if [ ! -d ${jourDir} ] ; then
    mkdir ${jourDir}
  fi
  cp -npvr *.geojson ${jourDir}
  cd ..
#  exit
  rm OBSP/*.geojson
  php rennesatlas.php GET action=clean_obsp
#  exit
#  ls -alrt OBSP/*.geojson
#  cp -pvr OBSP/*.geojson POINTOBS
#  WM ./OBSP ./POINTOBS/${jourDir}
#  exit
  cp -pvr ./OBSP/*.geojson ./POINTOBS
  LOG "OBSP_clean fin"
}
#f OBSP_rename:
OBSP_rename() {
  LOG "OBSP_rename debut"
  ncd rennesatlas
  cd POINTOBS
  for old in *_2018*.geojson; do
    new=$(echo $old | sed -e 's/_2018....//')
    mv -v "$old" "$new"
  done
  LOG "OBSP_rename fin"
}
#F POINTS:
POINTS() {
  LOG "POINTS debut"
  POINTS_r
  POINTS_tex
  LOG "POINTS fin"
}
#f POINTS_r:
POINTS_r() {
  LOG "POINTS_r debut"
#  _R install.R miscPkgs
  _R rennesatlas.R points_jour
  LOG "POINTS_r fin"
}
#f POINTS_tex:
POINTS_tex() {
  LOG "POINTS_tex debut"
  _TEX points
  _TEX precision
  LOG "POINTS_tex fin"
}
#F GIT: pour mettre à jour le dépot git
GIT() {
  LOG "GIT debut"
  Local="${DRIVE}/web/geo";  Depot=rennesatlas; Remote=frama
  export Local
  export Depot
  export Remote
  _git_lst
  bash ../win32/scripts/git.sh INIT $*
#  bash ../win32/scripts/git.sh PUSH
  LOG "GIT fin"
}
#f _git_lst: la liste des fichiers pour le dépot
_git_lst() {
  cat  <<'EOF' > /tmp/_git.lst
scripts/rennesatlas.sh
EOF
#  ls -1 scripts/rennesatlas*.R >> /tmp/git.lst
  _ENV_R
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/rennesatlas.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/rennesatlas_sc.R')" )
  cat /tmp/git.lst >> /tmp/_git.lst
  cat /tmp/_git.lst > /tmp/git.lst
#  exit;
#  ls -1 ${CFG}/*.tex >> /tmp/git.lst
  cat  <<'EOF' > /tmp/README.md
# rennesatlas : atlas des oiseaux de Rennes

Scripts en environnement Windows 10 : MinGW R MikTex

Ces scripts exploitent des données en provenance des bases Excel, Serena et Biolovision.

## Scripts R
Ces scripts sont dans le dossier "scripts".

## Tex
Le langage Tex est utilisé pour la production des documents.

MikTex est l'environnement utilisé.

Les fichiers .tex sont dans le dossier RENNESATLAS.
# fonctions
## production des fiches points d'écoute
### en R,
Pour chaque point d'écoute, on produit :
- un plan (extrait IGN gris)
- une vue aérienne (extrait IGN)
- un fichier Tex
et pour l'ensemble :
- un fichier Tex d'inclusion des fichiers Tex
- un script shell de découpe du pdf résultant par maille

### en Tex,
production du pdf

### en shell,
découpe du pdf

## production de la grille
### en R,
- fusion limite Rennes avec rocade + boulevard des alliés
- production d'un maillage englobant
- extraction des mailles ayant au moins 30% dans le périmètre
- numérotation

## production des points
### en R,
à partir de la grille,
- génération des 4 points
- générations des 4 cercles

## atlas qualitatif
### en R,
- extraction des données faune-bretagne,
- affectation des données par maille
- production carte nombre de données
EOF
}
#f TEX2ZIP:
TEX2ZIP() {
  LOG "TEX2ZIP debut"
  cd $CFG
  pwd
  zip="cartes.zip"
  rm $zip
  7z a ${zip} *.tex
  7z a ${zip} *.docx
  7z a ${zip} cartes/*.pdf
  7z l ${zip}
  LOG "TEX2ZIP fin"
}
[ $# -eq 0 ] && ( HELP )
CONF
while [ "$1" != "" ]; do
  case $1 in
    -c | --conf )
      shift
      Conf=$1
      ;;
    * )
      $*
      exit 1
  esac
  shift
done