# <!-- coding: utf-8 -->
#
# quelques fonction pour la météo
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# infoclimat
# https://www.infoclimat.fr/observations-meteo/archives/25/avril/2018/rennes-st-jacques/07130.html
#
# les pages web sont sauvegardées avec autoit
# elles sont ensuite analysées avec perl
#
# http://www.meteofrance.com/climat/france/rennes/35281001/releves
# rm(list=ls()); source("geo/scripts/meteo.R"); infoclimat_jour()
infoclimat_jour  <- function(force=FALSE) {
  infoclimat_verif()
}
# rm(list=ls()); source("geo/scripts/meteo.R"); infoclimat_lire()
infoclimat_lire  <- function(force=FALSE) {
  dsn <- sprintf("%s/%s.csv", varDir, 'infoclimat')
  dsn <- sprintf("d:/web.var/meteo/infoclimat/ie/%s.csv", 'VillesJoursValeurs')
  carp("dsn: %s", dsn)
  df <- read.table(dsn, header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote='"', encoding="UTF-8")
  carp("nrow: %s", nrow(df))
#  df$d <- as.Date(df$jjmmaaaa, "%d/%m/%Y")
  df$d <- as.Date(df$aaaammjj, "%Y/%m/%d")
  return(invisible(df))
}
#
# source("geo/scripts/meteo.R"); infoclimat_verif()
infoclimat_verif  <- function() {
  library(tidyverse)
  carp()
  df <- infoclimat_lire()
  carp("min: %s", min(df$d))
  carp("max: %s", max(df$d))
  jours.df <- data.frame(d=seq(min(df$d), max(df$d), by="days"))
  df1 <- jours.df %>%
    left_join(df, by="d") %>%
    filter(is.na(aaaammjj))
  if ( nrow(df1) > 0 ) {
    View(df1)
  }
}
# http://jeffgoldsmith.com/DSI/visualization.html
# source("geo/scripts/meteo.R"); infoclimat_stat()
infoclimat_stat  <- function() {
  carp()
  library(tidyverse)
  library(scales)
  library(lubridate)
  df <- infoclimat_lire()
  date_from <- as.Date("20/03/2018", "%d/%m/%Y")
  date_to <- as.Date("20/04/2018", "%d/%m/%Y")
  date_from <- as.Date("01/01/2018", "%d/%m/%Y")
  date_to <- as.Date("20/05/2018", "%d/%m/%Y")
  df <- df %>%
    filter(d >= date_from & d <= date_to) %>%
#    select(-Soleil) %>%
    glimpse()
#  infoclimat_stat_temp(df)
  infoclimat_stat_soleil(df)
}
infoclimat_stat_temp  <- function(df) {
  carp()
  ymin <- min(df$temp_min)
  ymax <- max(df$temp_max)
  ymin <- floor(ymin/5)*5
  ymax <- ceiling(ymax/5)*5
  ylegende <- "température °C"
  Encoding(ylegende) <- "UTF-8"
  glimpse(df)
  ggplot(df) +
    ylim(ymin, ymax) +
    geom_ribbon(aes(x = d, ymin = temp_min, ymax = temp_max), fill = "grey", alpha = 0.5) +
    geom_line(aes(x = d, y = temp_min), color = "blue") +
    geom_line(aes(x = d, y = temp_max), color = "red") +
    scale_x_date(date_breaks = "5 days", labels = date_format("%d-%m")) +
    ylab(ylegende) + theme(
      plot.title = element_blank(),
      axis.title.x = element_blank()
    )
}
infoclimat_stat_soleil  <- function(df) {
  carp()
  library(gridExtra)
  library(grid)
  library(scales)
# https://github.com/tidyverse/ggplot2/issues/2414
# http://ggplot2.tidyverse.org/reference/scale_date.html
# https://github.com/tidyverse/ggplot2/blob/master/R/scale-date.r
# http://steffi.ca/thinkR/tutorials/twoplots/
# http://zevross.com/blog/2014/08/04/beautiful-plotting-in-r-a-ggplot2-cheatsheet-3/
# https://stackoverflow.com/questions/3099219/plot-with-2-y-axes-one-y-axis-on-the-left-and-another-y-axis-on-the-right?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

  df$s <- gsub("h ", ":", df$ensoleillement)
  df$s <- gsub("min", "", df$s)
  df$Soleil <-  hms::parse_hm(df$s)
#  df$Soleil <- as.DateTime(df$s, "%H:%M")

  df$pluie <- as.numeric(gsub("mm", "", df$precipitations))
  ylegende <- "ensoleillement (hh:mm)"
  Encoding(ylegende) <- "UTF-8"
  glimpse(df)
  g.soleil <- ggplot2::ggplot(df) +
    geom_bar(aes(x = d, y = Soleil), stat="identity", fill = "yellow") +
    scale_x_date(date_breaks = "5 days", labels = date_format("%d-%m")) +
#    scale_y_date(date_breaks = "2 hours", labels = date_format("%H")) +
    ylab(ylegende) + theme(
#      plot.margin = unit(c(0,0,0,0),units="points"), #top, right, bottom, left
      plot.title = element_blank(),
      axis.title.x = element_blank()
    )
  ylegende <- "pluie (mm)"
  Encoding(ylegende) <- "UTF-8"
  g.pluie <- ggplot2::ggplot(df) +
    geom_bar(aes(x = d, y = pluie), stat="identity", fill = "blue") +
    scale_x_date(date_breaks = "5 days", labels = date_format("%d-%m")) +
    ylab(ylegende) + theme(
#      plot.margin = unit(c(5,0,-32,0),units="points"),
      plot.title = element_blank(),
      axis.title.x = element_blank()
    )
  grid.arrange(g.pluie, g.soleil, heights = c(1/5, 4/5))
#    theme(axis.text.x=element_text(angle=60, hjust=1))
}
# source("geo/scripts/meteo.R"); infoclimat_stat()
infoclimat_heure_stat  <- function() {
  library(tidyverse)
  library(scales)
  df <- infoclimat_lire()
  date_from <- as.Date("20/03/2018", "%d/%m/%Y")
  date_to <- as.Date("20/04/2018", "%d/%m/%Y")
  df1 <- df %>%
    filter(d >= date_from & d <= date_to) %>%
    group_by(d) %>%
    summarize(tmin=min(temperature), tmax=max(temperature))
  print(df1)
  ggplot(df1) +
    geom_point(aes(x = d, y = tmin), color = "blue") +
    geom_point(aes(x = d, y = tmax), color = "red")
}
# source("geo/scripts/meteo.R"); infoclimat_soleil()
infoclimat_heure_soleil  <- function() {
  library(tidyverse)
  library(suncalc)
# Rennes Saint-Jacques
  getSunlightTimes(date =  as.Date("20/04/2018", "%d/%m/%Y"), lat = 48.0647, lon = -1.72, tz='MET', keep=c('sunrise', 'sunset'))

}