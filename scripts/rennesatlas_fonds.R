# <!-- coding: utf-8 -->
#
# quelques fonction pour l'atlas Rennes : les fonds de carte
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# avec la rocade
fonds_rocade <- function() {
  carp()
  spdf <- couches_rocade_lire()
  plot(spdf, col = 'black', add=FALSE)
}
#
# pour les limites de l'atlas et le nombre de mailles
# source("geo/scripts/rennesatlas.R");fonds_atlas()
fonds_atlas <- function(pas=1000, maille=1) {
  carp()
  library(raster)
  couches_rasters_lire(force=TRUE)
  plotImg(imgGM13)
  rocade.spdf <- couches_rocade_lire()
  rennes.spdf <- couches_rennes_lire()
#  bb1 <- bbox(rocade.spdf)
#  bb2 <- bbox(rennes.spdf)
#  e <- extent(min(bb1[1,1], bb2[1,1]), max(bb1[1,2], bb2[1,2]), min(bb1[2,1], bb2[2,1]), max(bb1[2,2], bb2[2,2]))
  extent.spdf <- couches_extent()
#  plot(spdf, axes=FALSE,add=FALSE,type="n" ,xlab="",ylab="")
  plot(rocade.spdf, col = misc_col('yellow', 40),  lwd=3, add=TRUE)
  plot(rennes.spdf, border="green", lwd=3, add=TRUE)
  zone.spdf <- gUnion(rennes.spdf, rocade.spdf)
#  zone.spdf <- rennes.spdf
  grille.spdf <- grille_lambert2(extent.spdf, pas, align=TRUE, offset=FALSE)
#
# la version maille pleine
  if ( maille == 0 ) {
    grille.spdf$id <- seq(1, nrow(grille.spdf), 1)
    over <- rgeos::gIntersects(grille.spdf,zone.spdf, byid = TRUE)
    spdf <- grille.spdf[over[grille.spdf$id] > 0,]
  }
#
# la version avec l'intersection
  if ( maille == 1 ) {
    grille.spdf$id <- seq(1, nrow(grille.spdf), 1)
    spdf <- raster::intersect(grille.spdf, zone.spdf)
    spdf$surface <-  as.integer(rgeos::gArea(spdf, byid=TRUE))
# au moins 30% de la surface
    spdf <- spdf[spdf@data$surface >= pas*pas*.3, ]
#    View(spdf@data)
    spdf <-  grille.spdf[grille.spdf@data$id %in% spdf$id, ]
  }
  spdf$NUMERO <- sprintf("%02d", seq(1, nrow(spdf), 1))
  plot(spdf, border = 'red', lwd=2, add=TRUE)
  text(coordinates(spdf), labels=spdf@data$NUMERO, font=2)
#  View(spdf@data)
#  print(summary(spdf))
#  stoc.spdf <- couches_stoc_lire()
#  plot(stoc.spdf, col = 'darkred', pch=19, cex=2, add=TRUE)
#
# les points d'écoude
  df <- as.data.frame(coordinates(spdf))
  df$V1 <- df$V1 - 250
  df$V2 <- df$V2 - 250
  df$quadrant <- 'SW'
  df$NUMERO <- spdf$NUMERO
  points.df <- df
  df <- as.data.frame(coordinates(spdf))
  df$V1 <- df$V1 + 250
  df$V2 <- df$V2 - 250
  df$quadrant <- 'SE'
  df$NUMERO <- spdf$NUMERO
  points.df <- rbind(points.df, df)
    df <- as.data.frame(coordinates(spdf))
  df$V1 <- df$V1 + 250
  df$V2 <- df$V2 + 250
  df$quadrant <- 'NE'
  df$NUMERO <- spdf$NUMERO
  points.df <- rbind(points.df, df)
  df <- as.data.frame(coordinates(spdf))
  df$V1 <- df$V1 - 250
  df$V2 <- df$V2 + 250
  df$quadrant <- 'NW'
  df$NUMERO <- spdf$NUMERO
  points.df <- rbind(points.df, df)
  coordinates(points.df) = ~ V1 + V2
  points.spdf <- points.df
  points.spdf$source <- 'R'
#  View(points.spdf@data)
  points.spdf$numero <- sprintf("%s%s", points.spdf$NUMERO, points.spdf$quadrant)
  proj4string(points.spdf) <- proj4string(spdf)
  plot(points.spdf, col = 'darkred', pch=19, cex=2, add=TRUE)
  dsn <- sprintf('%s/grille_lambert_%s', varDir, pas)
  grille_sauve(spdf, dsn);
  dsn <- sprintf('%s/points_lambert_%s', varDir, pas)
  grille_sauve(points.spdf, dsn);
  cercles.spdf <- gBuffer(points.spdf, width=150, byid=TRUE)
  dsn <- sprintf('%s/cercles_lambert_%s', varDir, pas)
  grille_sauve(cercles.spdf, dsn);
#  View(points.df)
}
fonds_grille_lire <- function(force=TRUE) {
  if ( ! exists("grille.spdf") | force==TRUE) {
    dsn <- sprintf("%s/grille_lambert_1000.json", varDir)
    print(sprintf("fonds_grille_lire() dsn : %s", dsn))
    grille.spdf <<- ogr_lire(dsn)
  }
  return(grille.spdf)
}
fonds_grille_lire_sf <- function(force=TRUE, transform=2154) {
  if ( ! exists("grille.sf") | force==TRUE) {
    dsn <- sprintf("%s/grille_lambert_1000.json", varDir)
    carp(" dsn : %s", dsn)
    sf <- st_read(dsn)
    if(transform != FALSE) {
      sf <- sf %>%
        st_transform(., "+init=epsg:2154")
    }
    grille.sf <<- sf
  }
  return(grille.sf)
}
# rm(list=ls()); source("geo/scripts/rennesatlas.R");fonds_martinets_lire()
fonds_martinets_lire <- function(force=TRUE) {
  if ( ! exists("martinets.spdf") | force==TRUE) {
    dsn <- sprintf("%s/martinets/nids_martinets_geo_v2.geojson", varDir)
    carp("dsn : %s", dsn)
    martinets.spdf <<- ogr_lire(dsn)
  }
  return(invisible(martinets.spdf))
}
# fichier d'occupation des sols
# rm(list=ls()); source("geo/scripts/rennesatlas.R");fonds_oso_carte()
fonds_oso_carte <- function() {
# https://rpubs.com/bradleyboehmke/data_wrangling
  spdf <- fonds_grille_lire()
  if ( ! exists("osoRaster") ) {
    osoRaster <<- raster_oso_lire(spdf)
  }
  plot(osoRaster, axes=FALSE, add=FALSE)
  plot(spdf, add=TRUE, cex=1)
  text(coordinates(spdf), labels=spdf@data$NUMERO, font = 2, cex= 1,)
  scalebar(1000, type='bar', divs=2)
}