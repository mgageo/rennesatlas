# rennesatlas : atlas des oiseaux de Rennes

Scripts en environnement Windows 10 : MinGW R MikTex

Ces scripts exploitent des données en provenance des bases Excel, Serena et Biolovision.

## Scripts R
Ces scripts sont dans le dossier "scripts".

## Tex
Le langage Tex est utilisé pour la production des documents.

MikTex est l'environnement utilisé.

Les fichiers .tex sont dans le dossier RENNESATLAS.
# fonctions
## production des fiches points d'écoute
### en R,
Pour chaque point d'écoute, on produit :
- un plan (extrait IGN gris)
- une vue aérienne (extrait IGN)
- un fichier Tex
et pour l'ensemble :
- un fichier Tex d'inclusion des fichiers Tex
- un script shell de découpe du pdf résultant par maille

### en Tex,
production du pdf

### en shell,
découpe du pdf

## production de la grille
### en R,
- fusion limite Rennes avec rocade + boulevard des alliés
- production d'un maillage englobant
- extraction des mailles ayant au moins 30% dans le périmètre
- numérotation

## production des points
### en R,
à partir de la grille,
- génération des 4 points
- générations des 4 cercles

## atlas qualitatif
### en R,
- extraction des données faune-bretagne,
- affectation des données par maille
- production carte nombre de données
